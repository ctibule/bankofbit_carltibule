﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//added
using WindowsBankingApplication;

namespace TestProject
{
    [TestClass]
    public class BatchTest
    {
        [TestMethod]
        public void ProcessHeaderTest()
        {
            Batch batchObjTest = new Batch();
            PrivateObject batchObjPrivate = new PrivateObject(batchObjTest);

            //Expected and actual values
            bool actualValue = (bool)batchObjPrivate.Invoke("processHeader");

            Assert.AreEqual(true, actualValue);
        }
    }
}
