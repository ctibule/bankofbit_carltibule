﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    /// <summary>
    /// Exception used primarily for OnlineBanking website used when a non-logged in user accesses a page restricted
    /// for logged on users.
    /// </summary>
    public class NotLoggedInException : Exception
    {
        /// <summary>
        /// Constructs an instance of NotLoggedInException and sets a custom Exception message
        /// </summary>
        /// <param name="message">Message briefly describing the exception</param>
        public NotLoggedInException(string message) : base(message) { }


        /// <summary>
        /// Constructs an instance of NotLoggedInException and sets a default Exception message
        /// </summary>
        public NotLoggedInException() : this("You must be logged in to view this page.") { }
    }

    /// <summary>
    /// Exception thrown when no bank account has been selected/retrieved.
    /// </summary>
    public class NoBankAccountException : Exception
    {
        /// <summary>
        /// Constructs an instance of NoBankAccountSelectedException and sets a custom Exception message
        /// </summary>
        /// <param name="message">Message briefly describing the exception</param>
        public NoBankAccountException(string message) : base(message) { }

        /// <summary>
        /// Constructs an instance of NoBankAccountSelectedException and sets a default Exception message
        /// </summary>
        public NoBankAccountException() : this("A bank account must be selected") { }
    }

    /// <summary>
    /// Exception used primarily for wfTransactionPage when the amount entered to be used for transaction is greater than the 
    /// balance of the bank account
    /// </summary>
    public class InsufficientFundsException : Exception
    {
        /// <summary>
        /// Constructs an instance of InsufficientFundsException and sets a custom Exception message
        /// </summary>
        /// <param name="message">Message briefly describing the exception</param>
        public InsufficientFundsException(string message) : base(message) { }

        /// <summary>
        /// Constructs an instance of InsufficientFundsException and sets a default Exception message
        /// </summary>
        public InsufficientFundsException() : this("There is insufficient funds in the account to complete the transaction.") { }
    }

    /// <summary>
    /// Exception used when transaction applied to the bank account fails
    /// </summary>
    public class TransactionFailedException : Exception
    {
        /// <summary>
        /// Constructs an instance of TransactionFailedException and sets a custom Exception message
        /// </summary>
        /// <param name="message"></param>
        public TransactionFailedException(string message) : base(message) { }

        /// <summary>
        /// Constructs an instance of TransactionFailedException and sets a default Exception message
        /// </summary>
        public TransactionFailedException() : this("Transaction Failed") { }
    }

    /// <summary>
    /// Used Primarily for frmClients under WindowsBankingApplication when user enters a client number that does not exist in the
    /// database
    /// </summary>
    public class InvalidClientException : Exception
    {
        /// <summary>
        /// Constructs an instance of InvalidClientException and sets a custom Exception message
        /// </summary>
        /// <param name="message">Message briefly describing the exception</param>
        public InvalidClientException(string message) : base(message){}

        /// <summary>
        /// Constructs an instance of InvalidClientException and sets a default Exception message
        /// </summary>
        public InvalidClientException() : this("No Client is associated with the Client Number specified.") { }
    }

    /// <summary>
    /// Used when the XML file is invalid
    /// </summary>
    public class InvalidXMLFileException : Exception
    {
        /// <summary>
        /// Constructs an instance of InvalidXMLFileException and sets a custom Exception message
        /// </summary>
        /// <param name="message">Message briefly describing the exception</param>
        public InvalidXMLFileException(string message) : base(message) { }

        /// <summary>
        /// Constructs an instance of InvalidXMLFileException and sets a default Exception message
        /// </summary>
        public InvalidXMLFileException() : this("Invalid attribute for the XML file's root element.") { }
    }
}
