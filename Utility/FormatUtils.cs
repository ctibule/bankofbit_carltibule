﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public static class FormatUtils
    {
        /// <summary>
        /// Removes a word from the string
        /// </summary>
        /// <param name="uneditedString">the string where a word will be removed</param>
        /// <param name="stringToBeCleared">the string that will be removed</param>
        /// <returns>The word that is needed to be returned</returns>
        public static string RemoveWord(string uneditedString, string stringToBeCleared)
        {
            return uneditedString.Split(new string[] { stringToBeCleared }, StringSplitOptions.None)[0];
        }
    }
}
