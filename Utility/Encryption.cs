﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//added
using System.IO;
using System.Security.Cryptography;

namespace Utility
{
    /// <summary>
    /// Contains functions that support encryption operations
    /// </summary>
    public static class Encryption
    {
        /// <summary>
        /// Encrypts a file
        /// </summary>
        /// <param name="unencryptedFileName">Name of the unencrypted file</param>
        /// <param name="encryptedFileName">Name of the encrypted file</param>
        /// <param name="key">Key used to encrypt the file</param>
        public static void Encrypt(string unencryptedFileName, string encryptedFileName, string key)
        {
            //Declare the unencrypted and encrypted files
            FileStream unencryptedFile = new FileStream(unencryptedFileName, FileMode.Open, FileAccess.Read);
            FileStream encryptedFile = new FileStream(encryptedFileName, FileMode.Create, FileAccess.Write);

            //Create a DESCryptoServiceProvider object
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes(key);
            cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes(key);

            //Create an ICryptoTransform object
            ICryptoTransform cryptoTransform = cryptoProvider.CreateEncryptor();

            //Create CryptoStream object
            CryptoStream cryptoStream = new CryptoStream(encryptedFile, cryptoTransform, CryptoStreamMode.Write);

            //Define byte array and populate it with the contents of the unencrypted file
            byte[] byteArray = new byte[unencryptedFile.Length];
            unencryptedFile.Read(byteArray, 0, byteArray.Length);

            //Write the encrypted data to the CryptoStream object
            cryptoStream.Write(byteArray, 0, byteArray.Length);

            //Close the cryptostream and the files.
            cryptoStream.Close();
            unencryptedFile.Close();
            encryptedFile.Close();
        }

        /// <summary>
        /// Decrypts a file
        /// </summary>
        /// <param name="encryptedFileName">File path to the encrypted file</param>
        /// <param name="unencryptedFileName">File path to the unencrypted file</param>
        /// <param name="key">Key used to decrypt the file</param>
        public static void Decrypt(string encryptedFileName, string unencryptedFileName, string key)
        {
            //Declare a new StreamWriter object
            StreamWriter writer = new StreamWriter(unencryptedFileName);

            try
            {
                //Create a DESCryptoServiceProvider object
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes(key);
                cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes(key);

                //Create an encrypted file
                FileStream encryptedFile = new FileStream(encryptedFileName, FileMode.Open, FileAccess.Read);

                //Create ICryptoTransform object
                ICryptoTransform cryptoTransform = cryptoProvider.CreateDecryptor();

                //Create a CryptoStream object
                CryptoStream cryptoStream = new CryptoStream(encryptedFile, cryptoTransform, CryptoStreamMode.Read);

                //Write new decrypted file
                StreamReader reader = new StreamReader(cryptoStream);
                writer.Write(reader.ReadToEnd());

                //Flush the streamwriter object, close the StreamReader
                reader.Close();
                writer.Flush();
                writer.Close();
            }
            catch (Exception)
            {
                //Close decrypted StreamWriter
                writer.Close();

                //Delete the unencrypted file produced by the StreamWriter
                File.Delete(unencryptedFileName);

                //Throw an Exception with appropriate error message
                throw new Exception("Error occured while decrypting the file.");
            }

        }
    }
}
