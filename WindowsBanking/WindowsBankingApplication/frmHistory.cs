﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
//note:  this needed to be done because during development
//ef released v. 6
//ef6 needed to add this
//using System.Data.Entity.Core;

//added
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models;
using Utility;

namespace WindowsBankingApplication
{
    public partial class frmHistory : Form
    {
        //private instance of data context object
        BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        ///given:  client and bankaccount data will be retrieved
        ///in this form and passed throughout application
        ///this object will be used to store the current
        ///client and selected bankaccount
        ConstructorData constructorData;

        public frmHistory()
        {
            InitializeComponent();
        }

        /// <summary>
        /// given:  This constructor will be used when returning to frmClient
        /// from another form.  This constructor will pass back
        /// specific information about the client and bank account
        /// based on activites taking place in another form
        /// </summary>
        /// <param name="client">specific client instance</param>
        /// <param name="account">specific bank account instance</param>
        public frmHistory(ConstructorData constructorData)
        {
            InitializeComponent();
            this.constructorData = constructorData;

            //Set binding sources
            clientBindingSource.DataSource = constructorData.Client;
            bankAccountBindingSource.DataSource = constructorData.BankAccount;
        }

        /// <summary>
        /// given:  this code will navigate back to frmClient with
        /// the specific client and account data that launched
        /// this form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkReturn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //return to client with the data selected for this form
            frmClients frmClients = new frmClients(constructorData);
            frmClients.MdiParent = this.MdiParent;
            frmClients.Show();
            this.Close();
        }

        /// <summary>
        /// given - further code required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmHistory_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);

            try
            {
                //Set mask of AccountNumber's MaskedLabel control
                accountNumberMaskedLabel.Mask = BusinessRules.AccountFormat(this.constructorData.BankAccount.Description);

                //Set DataSource of DataGridView
                var transactions = from Transactions in db.Transactions
                                   join TransactionTypes in db.TransactionTypes
                                   on Transactions.TransactionTypeId equals TransactionTypes.TransactionTypeId
                                   where Transactions.BankAccountId == constructorData.BankAccount.BankAccountId
                                   select new
                                   {
                                       DateCreated = Transactions.DateCreated,
                                       TransactionType = TransactionTypes.Description,
                                       Deposit = Transactions.Deposit,
                                       Withdrawal = Transactions.Withdrawal,
                                       Notes = Transactions.Notes
                                   };

                transactionBindingSource.DataSource = transactions.ToList();

                //Format columns
                transactionDataGridView.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                transactionDataGridView.Columns[2].DefaultCellStyle.Format = "C";
                transactionDataGridView.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                transactionDataGridView.Columns[3].DefaultCellStyle.Format = "C";
               
            }
            catch (Exception ex)
            {
                //Show the message
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
