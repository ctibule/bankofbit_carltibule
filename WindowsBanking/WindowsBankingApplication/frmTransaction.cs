﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//note:  this needed to be done because during development
//ef released v. 6
//ef6 needed to add this
//using System.Data.Entity.Core;

//added
using Utility;
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;

namespace WindowsBankingApplication
{
    public partial class frmTransaction : Form
    {

        //Added: Private DB instance
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        ///given:  client and bankaccount data will be retrieved
        ///in this form and passed throughout application
        ///this object will be used to store the current
        ///client and selected bankaccount
        ConstructorData constructorData;

        public frmTransaction()
        {
            InitializeComponent();
        }

        /// <summary>
        /// given:  This constructor will be used when returning to frmClient
        /// from another form.  This constructor will pass back
        /// specific information about the client and bank account
        /// based on activites taking place in another form
        /// </summary>
        /// <param name="client">specific client instance</param>
        /// <param name="account">specific bank account instance</param>
        public frmTransaction(ConstructorData constructorData)
        {
            InitializeComponent();
            this.constructorData = constructorData;

            //Set data sources of clientBindingSource and bankAccountBindingSource
            clientBindingSource.DataSource = constructorData.Client;
            bankAccountBindingSource.DataSource = constructorData.BankAccount;
        }

        /// <summary>
        /// given: this code will navigate back to frmClient with
        /// the specific client and account data that launched
        /// this form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkReturn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //return to client with the data selected for this form
            frmClients frmClients = new frmClients(constructorData);
            frmClients.MdiParent = this.MdiParent;
            frmClients.Show();
            this.Close();

        }

        /// <summary>
        /// given:  further code required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTransaction_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);

            try
            {
                //Set masked label of the account number
                accountNumberMaskedLabel.Mask = BusinessRules.AccountFormat(constructorData.BankAccount.Description);

                //Set data source of transactionTypeDataSource (with Exceptions)
                transactionTypeComboBox.DataSource = (from x in db.TransactionTypes
                                                      where x.TransactionTypeId != (int)TransactionTypeValues.TransferRecipient &&
                                                            x.TransactionTypeId != (int)TransactionTypeValues.CalculateInterest
                                                      select x).ToList();

                //Add event listener to upper combobox
                transactionTypeComboBox.SelectedValueChanged += new EventHandler(transactionTypeComboBox_SelectedValueChanged);
            }
            catch (Exception ex)
            {
                //Show Exception
                MessageBox.Show("Error Occured", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Checks if there's sufficient funds in the bank account to proceed with the transaction
        /// </summary>
        /// <param name="amount">Amount involved in the transaction</param>
        /// <returns>True if there's sufficient balance, false if not.</returns>
        private bool checkFunds(double amount)
        {
            return this.constructorData.BankAccount.Balance > amount;
        }

        /// <summary>
        /// Event Handler for LinkedClicked event of the Process' LinkedLabel control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lnkProcess_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                //Check if txtAmount is numeric
                if (!Numeric.isNumeric(txtAmount.Text, System.Globalization.NumberStyles.None))
                {
                    throw new FormatException("A numeric value must be entered on the amount textbox");
                }
                //Check if there's sufficient funds in the bank account to proceed with the transaction if selected transaction is not deposit
                else if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) != (int)TransactionTypeValues.Deposit
                    && double.Parse(txtAmount.Text) > this.constructorData.BankAccount.Balance)
                {
                    throw new InsufficientFundsException();
                }
                //Proceed with the transaction
                else
                { 
                    //Declare instance of Transaction Manager
                    BankService.TransactionManagerClient transactionManager = new BankService.TransactionManagerClient();

                    //Declare values to be involved with the transaction
                    int sourceAccountId = this.constructorData.BankAccount.BankAccountId;
                    double amount = double.Parse(txtAmount.Text);
                    string notes = String.Empty;
                    double? updatedBalance = 0;

                    //Call the Deposit function from web service if selected transaction is deposit.
                    if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.Deposit)
                    { 
                        //Change the notes
                        notes = string.Format("Deposited {0:C} to {1}", amount, this.constructorData.BankAccount.AccountNumber);

                        //Perform the transaction
                        updatedBalance = transactionManager.Deposit(sourceAccountId, amount, notes);
                    }
                    //Call the Withdraw function from web service if selected transaction is withdrawal
                    else if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.Withdrawal)
                    { 
                        //Change the notes
                        notes = string.Format("Withdrew {0:C} from {1}", amount, this.constructorData.BankAccount.AccountNumber);

                        //Perform the transaction
                        updatedBalance = transactionManager.Withdrawal(sourceAccountId, amount, notes);
                    }
                    //Call the BillPayment function from web service if selected transaction is bill payment
                    else if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.BillPayment)
                    { 
                        //Change the notes
                        notes = string.Format("Paid {0:C} to {1}", amount, cboAccountPayee.Text);

                        //Perform the transaction
                        updatedBalance = transactionManager.BillPayment(sourceAccountId, amount, notes);
                    }
                    //Call the Transfer function from web service if selected transaction is transfer
                    else if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.Transfer)
                    { 
                        //Get the destination BankAccountId
                        int destinationBankAccountId = int.Parse(cboAccountPayee.SelectedValue.ToString());

                        //Change the notes
                        notes = string.Format("Transferred {0:C} to {1} from {2}", amount, cboAccountPayee.Text, this.constructorData.BankAccount.AccountNumber);

                        //Perform the transaction
                        updatedBalance = transactionManager.Transfer(sourceAccountId, destinationBankAccountId, amount, notes);
                    }

                    //Throw an exception if the updated balance is null, which means the Transaction failed
                    if (updatedBalance == null)
                    {
                        throw new TransactionFailedException();
                    }
                    //If the transaction succeeded, update balance label
                    else
                    {
                        balanceLabel1.Text = string.Format("{0:C}", updatedBalance);
                    }
                }
            }
            catch (Exception ex)
            {
                //Show the error
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Event Handler for the SelectedValueChanged event of TransactionTypeComboBox control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void transactionTypeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // Hide lblNoAccounts
                lblNoAccounts.Visible = false;

                //Disable cboAccountPayee
                lblAcctPayee.Visible = false;
                cboAccountPayee.Visible = false;

                //Bind cboPayees to Payee Entity classes if transaction type is bill payment
                if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.BillPayment)
                {
                    cboAccountPayee.DataSource = (from x in db.Payees
                                                  select x).ToList();

                    //Set DisplayMember and ValueMember
                    cboAccountPayee.DisplayMember = "Description";
                    cboAccountPayee.ValueMember = "PayeeId";

                    //Enable cboAccountPayee
                    lblAcctPayee.Visible = true;
                    cboAccountPayee.Visible = true;
                }
                //Bind cboPayees to BankAccounts that is associated with the selected client, with the exclusion of the current bank account
                else if (int.Parse(transactionTypeComboBox.SelectedValue.ToString()) == (int)TransactionTypeValues.Transfer)
                {
                    IQueryable<BankAccount> bankAccounts = from x in db.BankAccounts
                                                           where x.ClientId == constructorData.Client.ClientId &&
                                                           x.BankAccountId != constructorData.BankAccount.BankAccountId
                                                           select x;
                    
                    //Check if there's bank accounts associated with this client
                    if (bankAccounts.Any())
                    {
                        //Set data source for cboAccountPayee
                        cboAccountPayee.DataSource = bankAccounts.ToList();

                        //Set DisplayMember and ValueMember
                        cboAccountPayee.DisplayMember = "AccountNumber";
                        cboAccountPayee.ValueMember = "BankAccountId";

                        //Enable cboAccount Payee
                        lblAcctPayee.Visible = true;
                        cboAccountPayee.Visible = true;
                    }
                    else
                    {
                        lblNoAccounts.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
