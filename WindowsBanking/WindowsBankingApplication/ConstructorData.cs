﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Added
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
namespace WindowsBankingApplication
{
    /// <summary>
    /// given:TO BE MODIFIED
    /// this class is used to capture data to be passed
    /// among the windows forms
    /// </summary>
    public class ConstructorData
    {
        // ADDED @ 2017-10-30: Client and BankAccount properties 

        /// <summary>
        /// Represents a Client object
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// Represents a Bank Account object
        /// </summary>
        public BankAccount BankAccount { get; set; }
     }
}
