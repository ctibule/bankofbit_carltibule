﻿namespace WindowsBankingApplication
{
    partial class frmBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radSelect = new System.Windows.Forms.RadioButton();
            this.radAll = new System.Windows.Forms.RadioButton();
            this.lnkProcess = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.institutionComboBox = new System.Windows.Forms.ComboBox();
            this.institutionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.rtxtLog = new System.Windows.Forms.RichTextBox();
            this.grpTestEncryption = new System.Windows.Forms.GroupBox();
            this.radDecrypt = new System.Windows.Forms.RadioButton();
            this.radEncrypt = new System.Windows.Forms.RadioButton();
            this.lnkClear = new System.Windows.Forms.LinkLabel();
            this.btnTestEncryption = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.institutionBindingSource)).BeginInit();
            this.grpTestEncryption.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(415, 94);
            this.txtKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtKey.Name = "txtKey";
            this.txtKey.PasswordChar = '*';
            this.txtKey.Size = new System.Drawing.Size(132, 22);
            this.txtKey.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(444, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Enter Key:";
            // 
            // radSelect
            // 
            this.radSelect.AutoSize = true;
            this.radSelect.Location = new System.Drawing.Point(52, 53);
            this.radSelect.Margin = new System.Windows.Forms.Padding(4);
            this.radSelect.Name = "radSelect";
            this.radSelect.Size = new System.Drawing.Size(168, 21);
            this.radSelect.TabIndex = 1;
            this.radSelect.TabStop = true;
            this.radSelect.Text = "Select a Transmission";
            this.radSelect.UseVisualStyleBackColor = true;
            // 
            // radAll
            // 
            this.radAll.AutoSize = true;
            this.radAll.Checked = true;
            this.radAll.Location = new System.Drawing.Point(52, 23);
            this.radAll.Margin = new System.Windows.Forms.Padding(4);
            this.radAll.Name = "radAll";
            this.radAll.Size = new System.Drawing.Size(169, 21);
            this.radAll.TabIndex = 0;
            this.radAll.TabStop = true;
            this.radAll.Text = "Run All Transmissions";
            this.radAll.UseVisualStyleBackColor = true;
            // 
            // lnkProcess
            // 
            this.lnkProcess.AutoSize = true;
            this.lnkProcess.Location = new System.Drawing.Point(48, 140);
            this.lnkProcess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lnkProcess.Name = "lnkProcess";
            this.lnkProcess.Size = new System.Drawing.Size(164, 17);
            this.lnkProcess.TabIndex = 3;
            this.lnkProcess.TabStop = true;
            this.lnkProcess.Text = "Process Transmission(s)";
            this.lnkProcess.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkProcess_LinkClicked);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.institutionComboBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtKey);
            this.groupBox2.Controls.Add(this.radSelect);
            this.groupBox2.Controls.Add(this.radAll);
            this.groupBox2.Controls.Add(this.lnkProcess);
            this.groupBox2.Location = new System.Drawing.Point(25, 13);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(597, 177);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Transmission Selection";
            // 
            // institutionComboBox
            // 
            this.institutionComboBox.DataSource = this.institutionBindingSource;
            this.institutionComboBox.DisplayMember = "Description";
            this.institutionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.institutionComboBox.Enabled = false;
            this.institutionComboBox.FormattingEnabled = true;
            this.institutionComboBox.Location = new System.Drawing.Point(52, 94);
            this.institutionComboBox.Name = "institutionComboBox";
            this.institutionComboBox.Size = new System.Drawing.Size(222, 24);
            this.institutionComboBox.TabIndex = 5;
            this.institutionComboBox.ValueMember = "Description";
            // 
            // institutionBindingSource
            // 
            this.institutionBindingSource.DataSource = typeof(BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models.Institution);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 277);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Transmission Log:";
            // 
            // rtxtLog
            // 
            this.rtxtLog.HideSelection = false;
            this.rtxtLog.Location = new System.Drawing.Point(25, 298);
            this.rtxtLog.Margin = new System.Windows.Forms.Padding(4);
            this.rtxtLog.Name = "rtxtLog";
            this.rtxtLog.Size = new System.Drawing.Size(600, 253);
            this.rtxtLog.TabIndex = 10;
            this.rtxtLog.Text = "";
            // 
            // grpTestEncryption
            // 
            this.grpTestEncryption.Controls.Add(this.btnTestEncryption);
            this.grpTestEncryption.Controls.Add(this.radDecrypt);
            this.grpTestEncryption.Controls.Add(this.radEncrypt);
            this.grpTestEncryption.Location = new System.Drawing.Point(25, 197);
            this.grpTestEncryption.Name = "grpTestEncryption";
            this.grpTestEncryption.Size = new System.Drawing.Size(597, 77);
            this.grpTestEncryption.TabIndex = 11;
            this.grpTestEncryption.TabStop = false;
            this.grpTestEncryption.Text = "Test Encryption";
            // 
            // radDecrypt
            // 
            this.radDecrypt.AutoSize = true;
            this.radDecrypt.Location = new System.Drawing.Point(203, 35);
            this.radDecrypt.Name = "radDecrypt";
            this.radDecrypt.Size = new System.Drawing.Size(78, 21);
            this.radDecrypt.TabIndex = 1;
            this.radDecrypt.TabStop = true;
            this.radDecrypt.Text = "Decrypt";
            this.radDecrypt.UseVisualStyleBackColor = true;
            // 
            // radEncrypt
            // 
            this.radEncrypt.AutoSize = true;
            this.radEncrypt.Checked = true;
            this.radEncrypt.Location = new System.Drawing.Point(51, 35);
            this.radEncrypt.Name = "radEncrypt";
            this.radEncrypt.Size = new System.Drawing.Size(77, 21);
            this.radEncrypt.TabIndex = 0;
            this.radEncrypt.TabStop = true;
            this.radEncrypt.Text = "Encrypt";
            this.radEncrypt.UseVisualStyleBackColor = true;
            // 
            // lnkClear
            // 
            this.lnkClear.AutoSize = true;
            this.lnkClear.Location = new System.Drawing.Point(550, 555);
            this.lnkClear.Name = "lnkClear";
            this.lnkClear.Size = new System.Drawing.Size(72, 17);
            this.lnkClear.TabIndex = 12;
            this.lnkClear.TabStop = true;
            this.lnkClear.Text = "Clear Text";
            // 
            // btnTestEncryption
            // 
            this.btnTestEncryption.Location = new System.Drawing.Point(415, 34);
            this.btnTestEncryption.Name = "btnTestEncryption";
            this.btnTestEncryption.Size = new System.Drawing.Size(132, 23);
            this.btnTestEncryption.TabIndex = 2;
            this.btnTestEncryption.Text = "Test";
            this.btnTestEncryption.UseVisualStyleBackColor = true;
            // 
            // frmBatch
            // 
            this.ClientSize = new System.Drawing.Size(644, 599);
            this.Controls.Add(this.lnkClear);
            this.Controls.Add(this.grpTestEncryption);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rtxtLog);
            this.Name = "frmBatch";
            this.Text = "Bank Transmission";
            this.Load += new System.EventHandler(this.frmBatch_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.institutionBindingSource)).EndInit();
            this.grpTestEncryption.ResumeLayout(false);
            this.grpTestEncryption.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radSelect;
        private System.Windows.Forms.RadioButton radAll;
        private System.Windows.Forms.LinkLabel lnkProcess;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rtxtLog;
        private System.Windows.Forms.ComboBox institutionComboBox;
        private System.Windows.Forms.BindingSource institutionBindingSource;
        private System.Windows.Forms.GroupBox grpTestEncryption;
        private System.Windows.Forms.RadioButton radDecrypt;
        private System.Windows.Forms.RadioButton radEncrypt;
        private System.Windows.Forms.LinkLabel lnkClear;
        private System.Windows.Forms.Button btnTestEncryption;
    }
}
