﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//added
using System.Reflection;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using Utility;
using System.Data.Objects.SqlClient;

namespace WindowsBankingApplication
{
    /// <summary>
    /// Contains functions used to validate a batch file and validate it
    /// </summary>
    public class Batch
    {
        //Private instance of the context class
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        //Properties used to validate a Batch File
        private string InputFileName { get; set; }
        private string LogFileName { get; set; }
        private string LogData { get; set; }

        /// <summary>
        /// Query the db for the ID of the bank account corresponding with the bank account number
        /// </summary>
        /// <param name="bankAccountNumber">Account number of the bank account</param>
        /// <returns>ID of the Bank Account</returns>
        private int getBankAccountId(long bankAccountNumber)
        {
            return db.BankAccounts.Where(x => x.AccountNumber == bankAccountNumber).Select(x => x.BankAccountId).SingleOrDefault();
        }

        /// <summary>
        /// This function will process all errors found within the current file being processed.
        /// </summary>
        /// <param name="beforeQuery">All of the transaction records in the XML file.</param>
        /// <param name="afterQuery">All of the transaction records that passed validation.</param>
        /// <param name="message">Brief description of the nature of the error.</param>
        private void processErrors(IEnumerable<XElement> beforeQuery, IEnumerable<XElement> afterQuery, string message)
        {
            IEnumerable<XElement> failedTransactions = beforeQuery.Except(afterQuery);
            
            //Loop through the failedTransaction
            foreach (XElement transaction in failedTransactions)
            {
                string line1 = "-------ERROR-------";
                string line2 = string.Format("{1}File: {0}{1}", this.InputFileName, Environment.NewLine);
                string line3 = string.Format("Institution: {0}{1}", transaction.Element("institution"), Environment.NewLine);
                string line4 = string.Format("Account Number: {0}{1}", transaction.Element("account_no"), Environment.NewLine);
                string line5 = string.Format("Transaction Type: {0}{1}", transaction.Element("type"), Environment.NewLine);
                string line6 = string.Format("Amount: {0}{1}", transaction.Element("amount"), Environment.NewLine);
                string line7 = string.Format("Notes: {0}{1}", transaction.Element("notes"), Environment.NewLine);
                string line8 = string.Format("Nodes: {0}{1}", transaction.Elements().Count(), Environment.NewLine);
                string line9 = string.Format("{0}{1}-------------------", message, Environment.NewLine);

                this.LogData += string.Format("{9}{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{9}",
                    line1, line2, line3, line4, line5, line6, line7, line8, line9, Environment.NewLine);
            }
        }

        /// <summary>
        /// This functions is used to process all valid transactions
        /// </summary>
        /// <param name="transactionRecords"></param>
        private void processTransactions(IEnumerable<XElement> transactionRecords)
        {
            BankService.TransactionManagerClient client = new BankService.TransactionManagerClient();

            //Loop through transaction records
            foreach (XElement transaction in transactionRecords)
            {
                //Get necessary values to complete the transaction
                int bankAccountId = getBankAccountId(long.Parse(transaction.Element("account_no").Value));
                double amount = double.Parse(transaction.Element("amount").Value);
                string transactionNote = transaction.Element("notes").Value;

                double oldBalance = db.BankAccounts.Where(x => x.BankAccountId == bankAccountId).Select(x => x.Balance).SingleOrDefault();
                double updatedBalance = 0;

                if (int.Parse(transaction.Element("type").Value) == (int)TransactionTypeValues.Withdrawal)
                {
                    updatedBalance = (double)client.Withdrawal(bankAccountId, amount, transactionNote);
                }
                else if (int.Parse(transaction.Element("type").Value) == (int)TransactionTypeValues.CalculateInterest)
                {
                    updatedBalance = (double)client.CalculateInterest(bankAccountId, transactionNote);
                }

                //If the returned value from the Web Service is null, the transaction failed.
                if (updatedBalance == null)
                {
                    this.LogData += string.Format("***Transaction Completed Unsuccessfully***{0}{0}", Environment.NewLine); ;
                }
                else
                {
                    this.LogData += string.Format("Transaction Completed Successfully: {0} {1:C} from account {2}{3}{3}",
                        int.Parse(transaction.Element("type").Value) == (int)TransactionTypeValues.Withdrawal ? "Withdrawal" : "Calculated Interest",
                        int.Parse(transaction.Element("type").Value) == (int)TransactionTypeValues.Withdrawal ? amount : updatedBalance - oldBalance,
                        long.Parse(transaction.Element("account_no").Value),
                        Environment.NewLine);
                }
            }
        }

        /// <summary>
        /// This function is used to verify the contents of the detail records in the input file.
        /// </summary>
        private void processDetails()
        {
            XDocument xmlFile = XDocument.Load(InputFileName);

            //Get all transactions from the XML file
            IEnumerable<XElement> transactions = xmlFile.Element("account_update").Elements("transaction").AsQueryable();

            //Get records that has 5 elements
            IEnumerable<XElement> recordsWithAllElements = transactions.Where(x => x.Descendants().Count() == 5);
            processErrors(transactions, recordsWithAllElements, "Invalid Record. Missing some information.");

            //Get records that has the correct institution number
            IEnumerable<XElement> recordsWithValidInstitutionNumber = recordsWithAllElements.Where(x => int.Parse(x.Element("institution").Value) ==
                                                                        int.Parse(xmlFile.Element("account_update").Attribute("institution").Value));
            processErrors(recordsWithAllElements, recordsWithValidInstitutionNumber, "Institution Number for this record does not match the institution number specified in the root element.");

            //Get records where type and amount nodes are numeric
            IEnumerable<XElement> recordsWithNumericTypeAndAmount = recordsWithValidInstitutionNumber.Where(x => Numeric.isNumeric(x.Element("type").Value, System.Globalization.NumberStyles.Number) == true &&
                                                                                                            Numeric.isNumeric(x.Element("amount").Value, System.Globalization.NumberStyles.Number) == true);
            processErrors(recordsWithValidInstitutionNumber, recordsWithNumericTypeAndAmount, "Transaction Type or Amount value is not numeric.");


            //Get records where the transaction type node value is equal to Withdrawal and CalculateInterest
            IEnumerable<XElement> recordsWithValidTransactionType = recordsWithNumericTypeAndAmount.Where(x => int.Parse(x.Element("type").Value) == (int)TransactionTypeValues.Withdrawal ||
                                                                                                            int.Parse(x.Element("type").Value) == (int)TransactionTypeValues.CalculateInterest);
            processErrors(recordsWithNumericTypeAndAmount, recordsWithValidTransactionType, "Invalid Transaction Type.");

            //Get records with correct amount type for the transaction specified
            IEnumerable<XElement> recordsWithValidAmountForTransaction = recordsWithValidTransactionType.Where(x => (int.Parse(x.Element("type").Value) == (int)TransactionTypeValues.Withdrawal && double.Parse(x.Element("amount").Value) > 0) ||
                                                                                                                (int.Parse(x.Element("type").Value) == (int)TransactionTypeValues.CalculateInterest && double.Parse(x.Element("amount").Value) == 0));
            processErrors(recordsWithValidTransactionType, recordsWithValidAmountForTransaction, "Invalid amount value for transaction specified.");

            //Get records with valid account number
            IEnumerable<long> accountNumbers = db.BankAccounts.Select(x => x.AccountNumber);
            IEnumerable<XElement> recordsWithValidAccountNum = recordsWithValidAmountForTransaction.Where(x => accountNumbers.Any(y => x.Element("account_no").Value.Contains(y.ToString())));
            processErrors(recordsWithValidAmountForTransaction, recordsWithValidAccountNum, "Account number not found in database.");

            //Process valid transactions
            processTransactions(recordsWithValidAccountNum);
        }

        /// <summary>
        /// This function is used to verify the attributes of the XML file's root element. Transmission file will not
        /// be processed if any attributes produce an error.
        /// </summary>
        private bool processHeader()
        {
            try
            {
                XDocument xmlFile = XDocument.Load(InputFileName);
                XElement accountUpdateElement = xmlFile.Descendants("account_update").SingleOrDefault();
                bool isValidDate = true;
                bool isValidInstitutionNumber = true;
                bool isValidChecksum = true;

                int institutionNumber = int.Parse(accountUpdateElement.Attribute("institution").Value);

                //Check if the accountUpdateElement has 3 attributes
                if (accountUpdateElement.Attributes().Count() == 3)
                {
                    //Check if the date in the XML is equal to today's date
                    if (DateTime.Parse(accountUpdateElement.Attribute("date").Value) != DateTime.Today.Date)
                    {
                        isValidDate = false;
                        throw new InvalidXMLFileException("Root element's date attribute does not match today's date.");
                    }
                    //Check if the institution number is in the db
                    else if (db.Institutions.Where(x => x.InstitutionNumber == institutionNumber)
                                            .Select(x => x).SingleOrDefault() == null)
                    {
                        isValidInstitutionNumber = false;
                        throw new InvalidXMLFileException("Root element's institution attribute value not found in the database.");
                    }
                    //Check if the value of the checksum matches the sum of the account numbers in the XML file.
                    else if (accountUpdateElement.Descendants("account_no").Sum(x => long.Parse(x.Value)) != 
                                long.Parse(accountUpdateElement.Attribute("checksum").Value))
                    {
                        isValidChecksum = false;
                        throw new InvalidXMLFileException("Root element's checksum value is incorrect.");
                    }

                    //If all of the attributes are valid, return a true value
                    if (isValidDate && isValidInstitutionNumber && isValidChecksum)
                    {
                        return true;
                    }
                    else
                    {
                        throw new InvalidXMLFileException("Invalid attributes may have been found on the XML file's root element.");
                    }
                }
                //Throw an exception if the root element does not have 3 elements
                else
                {
                    throw new InvalidXMLFileException(string.Format("Root element {0} does not have 3 attributes", accountUpdateElement.Name));
                }
            }
            catch (Exception ex)
            {
                //Log error message
                this.LogData += string.Format("{0}{1}", ex.Message, Environment.NewLine);

                return false;
            }
        }

        /// <summary>
        /// This function will be called when a file has been processed
        /// </summary>
        /// <returns></returns>
        public string WriteLogData()
        {
            //Check if the original file name exists and if it does, rename it.
            if (File.Exists(this.InputFileName))
            {
                //Rename the XML file
                File.Move(this.InputFileName, string.Format("COMPLETE-{0}", this.InputFileName));
            }

            //Check if the COMPLETE file exists and if it does, delete it.
            if (File.Exists(string.Format("COMPLETE-{0}", this.InputFileName)))
            {
                File.Delete(string.Format("COMPLETE-{0}", this.InputFileName));
            }

            //Write the Log Data to the file
            FileStream logFile = new FileStream(this.LogFileName, FileMode.Create);
            StreamWriter writer = new StreamWriter(logFile);
            writer.Write(this.LogData);
            writer.Flush();
            writer.Close();

            //Capture contents of logData
            string logDataContents = this.LogData;

            //Clear the LogData and LogFileName
            this.LogData = string.Empty;
            this.LogFileName = string.Empty;

            //Return contents of logging data
            return logDataContents;
        }

        /// <summary>
        /// This function will initiate the bank transmission process by determining the appropriate file name and then proceeding 
        /// with the header and detail processing
        /// </summary>
        /// <param name="institution">*ENTER DESCRIPTION HERE*</param>
        /// <param name="key">*ENTER DESCRIPTION HERE*</param>
        public void ProcessTransmission(string institution, string key)
        { 
            //Set the Log Data, Input and Encrypted file name
            this.InputFileName = string.Format("{0}-{1}-{2}.xml", DateTime.Today.Year, DateTime.Today.DayOfYear, institution);
            this.LogFileName = string.Format("LOG {0}-{1}-{2}.txt", DateTime.Today.Year, DateTime.Today.DayOfYear, institution);
            string encryptedFileName = string.Format("{0}.encrypted", this.InputFileName);

            //Encryption process starts here. Exceptions will be caught and errors will be appended to log file.
            try
            {
                //Check if the encrypted file exists and if it does, decrypt it
                if(File.Exists(encryptedFileName))
                {
                    Encryption.Decrypt(encryptedFileName, this.InputFileName, key);

                    //Check if the  decrypted file doesn't exist. Append appropriate error message to the log file.
                    if (!File.Exists(this.InputFileName))
                    {
                        //Log the error to an Error Log File
                        LogData += String.Format("{0} batch file does not exist.{1}", InputFileName, Environment.NewLine);
                    }
                    //If the decrypted file exist, continue with the processing.
                    else
                    {
                        if (processHeader())
                        {
                            processDetails();
                        }
                    }
                }
                //Check if the encrypted file doesn't exist. Log a message if it doesn't exist. 
                else if (!File.Exists(encryptedFileName))
                {
                    this.LogData += string.Format("Encrypted file does not exists.{0}", Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                this.LogData += string.Format("{0}{1}", ex.Message, Environment.NewLine);
            }
        }
    }
}
