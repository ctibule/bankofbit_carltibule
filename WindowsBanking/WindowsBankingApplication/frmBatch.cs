﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

//added
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using Utility;

namespace WindowsBankingApplication
{
    public partial class frmBatch : Form
    {
        //Private instance of db
        BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();
        public frmBatch()
        {
            InitializeComponent();
        }

        /// <summary>
        /// given - further code required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBatch_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);

            //Set datasource of the binding source
            institutionBindingSource.DataSource = db.Institutions.ToList();
            institutionComboBox.DisplayMember = "Description";
            institutionComboBox.ValueMember = "InstitutionNumber";

            //Event Handler for radSelect
            radSelect.CheckedChanged += delegate(object radSelectCheckedChangedSender, EventArgs radSelectCheckedChangedEvent)
                                        {
                                            if (radSelect.Checked)
                                            {
                                                institutionComboBox.Enabled = true;
                                            }
                                            else
                                            {
                                                institutionComboBox.Enabled = false;
                                            }
                                        };

            //Event Handler for lnkClear
            lnkClear.LinkClicked += delegate(object lnkClearLinkClickedSender, LinkLabelLinkClickedEventArgs lnkClearLinkClicked)
                                    {
                                        rtxtLog.Text = string.Empty;
                                    };

            //Event Handler for btnTestEncryption
            btnTestEncryption.Click += new EventHandler(btnTestEncryption_Click);
        }

        /// <summary>
        /// Encrypts or decrypts the file based on user selection
        /// </summary>
        /// <param name="institutionNumber"></param>
        /// <param name="key"></param>
        private void testEncryption(string institutionNumber, string key)
        {
            string decryptedFileName = string.Format("{0}-{1}-{2}.xml", DateTime.Today.Year, DateTime.Today.DayOfYear, institutionNumber);
            string encryptedFileName = string.Format("{0}.encrypted", decryptedFileName);

            //Clear the textbox
            rtxtLog.Text = "";

            //If "Encrypt" is selected
            if (radEncrypt.Checked)
            {
                //Delete the encrypted file if it exists
                if (File.Exists(encryptedFileName))
                {
                    File.Delete(encryptedFileName);
                }

                //Proceed with the encryption if the decrypted file exists
                if (File.Exists(decryptedFileName))
                {
                    //Encrypt decrypted file
                    Encryption.Encrypt(decryptedFileName, encryptedFileName, key);

                    //Append textbox with the contents of the encrypted file
                    StreamReader encryptedFileReader = new StreamReader(encryptedFileName);
                    rtxtLog.Text += encryptedFileReader.ReadToEnd();
                    
                    //Append a linebreak and separator
                    rtxtLog.Text += string.Format("{0}------------------------------------------------{0}", Environment.NewLine);

                    //Close the reader of the encrypted file
                    encryptedFileReader.Close();

                    //Delete the decrypted file
                    File.Delete(decryptedFileName);
                }
                else
                {
                    throw new Exception("A decrypted file must exist before encryption could be tested.");
                }
            }
            //Check if "Decrypted" is selected
            else if (radDecrypt.Checked)
            { 
                //Delete the decrypted file if it exists
                if (File.Exists(decryptedFileName))
                {
                    File.Delete(decryptedFileName);
                }

                //Proceed with the decryption if the encryption file exists
                if (File.Exists(encryptedFileName))
                { 
                    //Decrypt the encrypted file
                    Encryption.Decrypt(encryptedFileName, decryptedFileName, txtKey.Text.Trim());

                    //Append the textbox with the contents of the decrypted file
                    StreamReader decryptedFileReader = new StreamReader(decryptedFileName);
                    rtxtLog.Text += decryptedFileReader.ReadToEnd();

                    //Append a linebreak and separator
                    rtxtLog.Text += string.Format("{0}------------------------------------------------{0}", Environment.NewLine);

                    //Close the reader and delete the encrypted file
                    decryptedFileReader.Close();
                    File.Delete(encryptedFileName);
                }
                else
                {
                    throw new Exception("An encrypted file must exist before decryption could be tested.");
                }
            }
        }

        /// <summary>
        /// Event Handler for Click event for button encryption
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btnTestEncryption_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtKey.Text.Trim().Length != 8)
                {
                    throw new Exception("8-character encryption key must be provided.");
                }
                //If a specific institution has been selected by the user, test encryption on that file only.
                else if(radSelect.Checked)
                {     
                    testEncryption(institutionComboBox.SelectedValue.ToString(), txtKey.Text.Trim());
                }
                //If the user did not specify an institution, test encryption on all institution files
                else if (radAll.Checked)
                {
                    foreach (Institution institution in institutionComboBox.Items)
                    {
                        testEncryption(institution.InstitutionNumber.ToString(), txtKey.Text.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// given - further code required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkProcess_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                //Clear the textbox
                rtxtLog.Text = "";

                //given - for use in encryption assignment
                if (txtKey.Text.Trim().Length != 8)
                {
                    MessageBox.Show("64 Bit Decryption Key must be entered", "Enter Key");
                    txtKey.Focus();
                }

                //Added - evaluate which radio button is checked
                Batch batch = new Batch();

                if (radSelect.Checked)
                {
                    batch.ProcessTransmission(institutionComboBox.SelectedValue.ToString(), txtKey.Text.Trim());
                    rtxtLog.Text += batch.WriteLogData();
                }
                else if (radAll.Checked)
                {
                    foreach (Institution institution in institutionComboBox.Items)
                    {
                        batch.ProcessTransmission(institution.InstitutionNumber.ToString(), txtKey.Text.Trim());
                        rtxtLog.Text += batch.WriteLogData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
