﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using System.Data.Linq.Mapping;
//note:  this needed to be done because during development
//ef released v. 6
//ef6 needed to add this
//using System.Data.Entity.Core;

using System.IO.Ports;      //for rfid assignment

//Added 
using Utility;
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;

namespace WindowsBankingApplication
{
    public partial class frmClients : Form
    {
        //Private instance of DataContext object
        BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        ///given: client and bankaccount data will be retrieved
        ///in this form and passed throughout application
        ///these variables will be used to store the current
        ///client and selected bankaccount
        ConstructorData constructorData = new ConstructorData();


        public frmClients()
        {
            InitializeComponent();

            //Prevents Leave Event from firing when FormClosing event is fired
            this.FormClosing += delegate(object sender, FormClosingEventArgs e)
                                {
                                    clientNumberMaskedTextBox.Leave -= clientNumberMaskedTextBox_Leave;
                                }; 
        }

        /// <summary>
        /// given:  This constructor will be used when returning to frmClient
        /// from another form.  This constructor will pass back
        /// specific information about the client and bank account
        /// based on activites taking place in another form
        /// </summary>
        /// <param name="client">specific client instance</param>
        /// <param name="account">specific bank account instance</param>
        public frmClients(ConstructorData constructorData)
        {
            InitializeComponent();

            //further code to be added
            this.constructorData.Client = constructorData.Client;
            this.constructorData.BankAccount = constructorData.BankAccount;

            //Set value of client textbox and trigger the MaskedTextbox_Leave Event
            clientNumberMaskedTextBox.Text = this.constructorData.Client.ClientNumber.ToString();
            clientNumberMaskedTextBox_Leave(null, null);
        }

        /// <summary>
        /// given: open history form passing data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkDetails_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //instance of frmHistory passing constructor data
            frmHistory frmHistory = new frmHistory(constructorData);
            //open in frame
            frmHistory.MdiParent = this.MdiParent;
            //show form
            frmHistory.Show();
            this.Close();
        }

        /// <summary>
        /// given: open transaction form passing constructor data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkTransaction_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //instance of frmTransaction passing constructor data
            frmTransaction frmTransaction = new frmTransaction(constructorData);
            //open in frame
            frmTransaction.MdiParent = this.MdiParent;
            //show form
            frmTransaction.Show();
            this.Close();
        }

       /// <summary>
       /// given
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void frmClients_Load(object sender, EventArgs e)
        {
            //keeps location of form static when opened and closed
            this.Location = new Point(0, 0);

            //Triggered when selection in combobox changes
            accountNumberComboBox.SelectedValueChanged += new EventHandler(accountNumberComboBox_SelectedValueChanged);
        }

        /// <summary>
        /// Triggered when selected value in combobox changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void accountNumberComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (accountNumberComboBox.SelectedValue != null)
            {
                int bankAccountId = int.Parse(accountNumberComboBox.SelectedValue.ToString());

                //Get bank account from database
                BankAccount selectedBankAccount = (from x in db.BankAccounts
                                                   where x.BankAccountId == bankAccountId
                                                   select x).SingleOrDefault();

                //Set bank account of constructor data
                constructorData.BankAccount = selectedBankAccount;
            }
        }

        /// <summary>
        /// Triggered when focus leaves clientNumberMaskedTextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clientNumberMaskedTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Client retrievedClient = retrieveSelectedClient();

                //Set the data source of the clientBindingSource is the retrieved client is not null
                //Retrieve bank accounts associated with the client
                if (retrievedClient != null)
                {
                    clientBindingSource.DataSource = retrievedClient;

                    //Retrieve BankAccounts associated with the client
                    IQueryable<BankAccount> retrievedAccounts = from x in db.BankAccounts
                                                                where x.ClientId == retrievedClient.ClientId
                                                                select x;

                    

                    //Set the data source of the bankAccount binding source if the retrievedAccounts has elements inside it
                    if (retrievedAccounts.Any())
                    {
                        bankAccountBindingSource.DataSource = retrievedAccounts.ToList();

                        //Set data source of account number combobox
                        accountNumberComboBox.DataSource = bankAccountBindingSource;

                        //Set text property of the combobox equal to the account number of the account number
                        if (constructorData.BankAccount != null)
                        {
                            accountNumberComboBox.Text = constructorData.BankAccount.AccountNumber.ToString();
                        }

                        //Enable links
                        lnkTransaction.Enabled = true;
                        lnkDetails.Enabled = true;

                        //Added: Add Selected Client to constructorData
                        constructorData.Client = retrievedClient;
                    }
                    //If there's no elements inside retrievedAccounts, throw an Exception
                    else
                    {
                        throw new NoBankAccountException("No bank account is associated with this client.");
                    }

                }
                //If retrieved client is null, throw an InvalidClient Exception
                else
                {
                    throw new InvalidClientException();
                }
            }
            catch (Exception ex)
            {
                //Disable linked labels at the bottom of the form
                lnkTransaction.Enabled = false;
                lnkDetails.Enabled = false;

                //Clear bank account binding source
                bankAccountBindingSource.DataSource = typeof(BankAccount);
                bankAccountBindingSource.ResetBindings(true);

                //Clear client binding source only if the exception is of InvalidClientException type
                if (ex is InvalidClientException)
                {
                    //Clear Binding Sources and refresh it to clear display
                    clientBindingSource.DataSource = typeof(Client);
                    clientBindingSource.ResetBindings(true);
                }

                //Show the message
                MessageBox.Show(ex.Message, "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Gets the Client object from the database based on the Client number inputted by the user
        /// </summary>
        /// <returns></returns>
        private Client retrieveSelectedClient()
        {
            //Get client number
            int clientNumber = int.Parse(Numeric.ClearFormatting(clientNumberMaskedTextBox.Text, "-"));

            //Set dataSource
            Client retrievedClient = (from x in db.Clients
                                      where x.ClientNumber == clientNumber
                                      select x).SingleOrDefault();
            return retrievedClient;
        }
    }
}
