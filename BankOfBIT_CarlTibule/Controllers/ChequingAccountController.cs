﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models;

namespace BankOfBIT_CarlTibule.Controllers
{
    /**
     * NOTE:
     * [2017-09-04]
     * - A line of code in Index() function have been modified to restrict rows being returned to only those belonging to ChequingAccounts table.
     * - Several lines of code have been modified to cast query being returned to ChequingAccount object.
     * 
     * [2017-09-05] Edited several lines of code to show full name and Account State Description for drop-down controls
     * 
     * [2017-09-17] (MileStone) Edited [HttpPost] Create() and [HttpPost] Edit() functions to call ChangeState() function.
     * 
     * [2017-09-25] Milestone #4: Modified [HTTPost] Create() function to call SetNextAccountNumber() function
     **/

    public class ChequingAccountController : Controller
    {
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        //
        // GET: /ChequingAccount/

        public ActionResult Index()
        {
            // var bankaccounts = db.BankAccounts.Include(c => c.Client).Include(c => c.AccountState);
            var bankaccounts = db.ChequingAccounts.Include(c => c.Client).Include(c => c.AccountState);
            return View(bankaccounts.ToList());
        }

        //
        // GET: /ChequingAccount/Details/5

        public ActionResult Details(int id = 0)
        {
            ChequingAccount chequingaccount = (ChequingAccount)db.BankAccounts.Find(id);
            if (chequingaccount == null)
            {
                return HttpNotFound();
            }
            return View(chequingaccount);
        }

        //
        // GET: /ChequingAccount/Create

        public ActionResult Create()
        {
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName");
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description");
            return View();
        }

        //
        // POST: /ChequingAccount/Create

        [HttpPost]
        public ActionResult Create(ChequingAccount chequingaccount)
        {
            //Added: Assigns account number to chequing account before it is added to the database
            chequingaccount.SetNextAccountNumber();

            if (ModelState.IsValid)
            {
                db.BankAccounts.Add(chequingaccount);
                db.SaveChanges();

                //Added
                chequingaccount.ChangeState();
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", chequingaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", chequingaccount.AccountStateId);
            return View(chequingaccount);
        }

        //
        // GET: /ChequingAccount/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ChequingAccount chequingaccount = (ChequingAccount)db.BankAccounts.Find(id);
            if (chequingaccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", chequingaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", chequingaccount.AccountStateId);
            return View(chequingaccount);
        }

        //
        // POST: /ChequingAccount/Edit/5

        [HttpPost]
        public ActionResult Edit(ChequingAccount chequingaccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chequingaccount).State = EntityState.Modified;
                db.SaveChanges();

                //Added
                chequingaccount.ChangeState();
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", chequingaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", chequingaccount.AccountStateId);
            return View(chequingaccount);
        }

        //
        // GET: /ChequingAccount/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ChequingAccount chequingaccount = (ChequingAccount)db.BankAccounts.Find(id);
            if (chequingaccount == null)
            {
                return HttpNotFound();
            }
            return View(chequingaccount);
        }

        //
        // POST: /ChequingAccount/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ChequingAccount chequingaccount = (ChequingAccount)db.BankAccounts.Find(id);
            db.BankAccounts.Remove(chequingaccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}