﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models;

namespace BankOfBIT_CarlTibule.Controllers
{
    /**
     * NOTE: 
     * - A line of code under Index() function has been modified to specify that only rows from BronzeStates table must be returned.
     * - Several lines of code have been modified to specify that BronzeState type must be returned when querying for ID.
     * [16 Sept. 2017]
     * - Replaced Index() function according to Milestone #3 instruction
     **/

    public class BronzeStateController : Controller
    {
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        //
        // GET: /BronzeState/

        public ActionResult Index()
        {
            //return View(db.BronzeStates.ToList());
            return View(BronzeState.GetInstance());
        }

        //
        // GET: /BronzeState/Details/5

        public ActionResult Details(int id = 0)
        {
            BronzeState bronzestate = (BronzeState)db.AccountStates.Find(id);
            if (bronzestate == null)
            {
                return HttpNotFound();
            }
            return View(bronzestate);
        }

        //
        // GET: /BronzeState/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BronzeState/Create

        [HttpPost]
        public ActionResult Create(BronzeState bronzestate)
        {
            if (ModelState.IsValid)
            {
                db.AccountStates.Add(bronzestate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bronzestate);
        }

        //
        // GET: /BronzeState/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BronzeState bronzestate = (BronzeState)db.AccountStates.Find(id);
            if (bronzestate == null)
            {
                return HttpNotFound();
            }
            return View(bronzestate);
        }

        //
        // POST: /BronzeState/Edit/5

        [HttpPost]
        public ActionResult Edit(BronzeState bronzestate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bronzestate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bronzestate);
        }

        //
        // GET: /BronzeState/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BronzeState bronzestate = (BronzeState)db.AccountStates.Find(id);
            if (bronzestate == null)
            {
                return HttpNotFound();
            }
            return View(bronzestate);
        }

        //
        // POST: /BronzeState/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BronzeState bronzestate = (BronzeState)db.AccountStates.Find(id);
            db.AccountStates.Remove(bronzestate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}