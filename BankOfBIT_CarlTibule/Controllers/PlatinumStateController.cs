﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models;

namespace BankOfBIT_CarlTibule.Controllers
{
    /**
     * NOTES:
     * 4 September 2017
     * - A line of code under Index() function have been modified to limit rows being returned to those belonging to PlatinumStates table.
     * - Several lines of code have been modified to cast query being returned to PlatinumState object.
     * [16 Sept. 2017]
     * - Code under Index() function have been modified according to Milestone 3 instructions.
     **/

    public class PlatinumStateController : Controller
    {
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        //
        // GET: /PlatinumState/

        public ActionResult Index()
        {
            //return View(db.PlatinumStates.ToList());
            return View(PlatinumState.GetInstance());
        }

        //
        // GET: /PlatinumState/Details/5

        public ActionResult Details(int id = 0)
        {
            PlatinumState platinumstate = (PlatinumState)db.AccountStates.Find(id);
            if (platinumstate == null)
            {
                return HttpNotFound();
            }
            return View(platinumstate);
        }

        //
        // GET: /PlatinumState/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlatinumState/Create

        [HttpPost]
        public ActionResult Create(PlatinumState platinumstate)
        {
            if (ModelState.IsValid)
            {
                db.AccountStates.Add(platinumstate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(platinumstate);
        }

        //
        // GET: /PlatinumState/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlatinumState platinumstate = (PlatinumState)db.AccountStates.Find(id);
            if (platinumstate == null)
            {
                return HttpNotFound();
            }
            return View(platinumstate);
        }

        //
        // POST: /PlatinumState/Edit/5

        [HttpPost]
        public ActionResult Edit(PlatinumState platinumstate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(platinumstate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(platinumstate);
        }

        //
        // GET: /PlatinumState/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlatinumState platinumstate = (PlatinumState)db.AccountStates.Find(id);
            if (platinumstate == null)
            {
                return HttpNotFound();
            }
            return View(platinumstate);
        }

        //
        // POST: /PlatinumState/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PlatinumState platinumstate = (PlatinumState)db.AccountStates.Find(id);
            db.AccountStates.Remove(platinumstate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}