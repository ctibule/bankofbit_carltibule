﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models;

namespace BankOfBIT_CarlTibule.Controllers
{
    /**
     * NOTE:
     * 
     * [2017-09-04]
     * - A line of code under Index() function have been modified to limit rows returned to those belonging to SavingsAccounts table
     * - Several lines of code have been modified to cast query being returned to SavingsAccount object
     * 
     * [2017-09-05]
     * - Edited to show full name and account state description on drop-down controls
     * 
     * [2017-09-17]
     * - (MileStone) Edited [HttpPost] Create() and [HttpPost] Edit() functions to call ChangeState() function.
     * 
     * [2017-09-25] Modified [HttpPost] Create() function to call SetNextAccountNumber() function
     **/

    public class SavingsAccountController : Controller
    {
        private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

        //
        // GET: /SavingsAccount/

        public ActionResult Index()
        {
            //var bankaccounts = db.BankAccounts.Include(s => s.Client).Include(s => s.AccountState);
            var bankaccounts = db.SavingsAccounts.Include(s => s.Client).Include(s => s.AccountState);
            return View(bankaccounts.ToList());
        }

        //
        // GET: /SavingsAccount/Details/5

        public ActionResult Details(int id = 0)
        {
           SavingsAccount savingsaccount = (SavingsAccount)db.BankAccounts.Find(id);
            if (savingsaccount == null)
            {
                return HttpNotFound();
            }
            return View(savingsaccount);
        }

        //
        // GET: /SavingsAccount/Create

        public ActionResult Create()
        {
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName");
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description");
            return View();
        }

        //
        // POST: /SavingsAccount/Create

        [HttpPost]
        public ActionResult Create(SavingsAccount savingsaccount)
        {
            //Added: Assigns account number to savings account before it is added to the database
            savingsaccount.SetNextAccountNumber();

            if (ModelState.IsValid)
            {
                db.BankAccounts.Add(savingsaccount);
                db.SaveChanges();

                //Added
                savingsaccount.ChangeState();
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", savingsaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", savingsaccount.AccountStateId);
            return View(savingsaccount);
        }

        //
        // GET: /SavingsAccount/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SavingsAccount savingsaccount = (SavingsAccount)db.BankAccounts.Find(id);
            if (savingsaccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", savingsaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", savingsaccount.AccountStateId);
            return View(savingsaccount);
        }

        //
        // POST: /SavingsAccount/Edit/5

        [HttpPost]
        public ActionResult Edit(SavingsAccount savingsaccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(savingsaccount).State = EntityState.Modified;
                db.SaveChanges();

                //Added
                savingsaccount.ChangeState();
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "FullName", savingsaccount.ClientId);
            ViewBag.AccountStateId = new SelectList(db.AccountStates, "AccountStateId", "Description", savingsaccount.AccountStateId);
            return View(savingsaccount);
        }

        //
        // GET: /SavingsAccount/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SavingsAccount savingsaccount = (SavingsAccount)db.BankAccounts.Find(id);
            if (savingsaccount == null)
            {
                return HttpNotFound();
            }
            return View(savingsaccount);
        }

        //
        // POST: /SavingsAccount/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            SavingsAccount savingsaccount = (SavingsAccount)db.BankAccounts.Find(id);
            db.BankAccounts.Remove(savingsaccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}