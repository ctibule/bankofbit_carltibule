namespace BankOfBIT_CarlTibule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccounts", "Amortization", c => c.Int());
            DropColumn("dbo.BankAccounts", "Amortizaton");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BankAccounts", "Amortizaton", c => c.Int());
            DropColumn("dbo.BankAccounts", "Amortization");
        }
    }
}
