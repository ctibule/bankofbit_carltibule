/**
 * CHANGELOG:
 * [2017-09-24] Modified to add SQL files under SQL folder as StoredProcedures in SQLServer.
 **/

namespace BankOfBIT_CarlTibule.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoredProcedure : DbMigration
    {
        public override void Up()
        {
            //Call Script to recreate the stored procedure
            this.Sql(Properties.Resources.create_next_number);
        }
        
        public override void Down()
        {
            //Call Script to drop the stored procedure
            this.Sql(Properties.Resources.drop_next_number);
        }
    }
}
