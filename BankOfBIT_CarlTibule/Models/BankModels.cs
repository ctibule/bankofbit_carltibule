﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankOfBIT_CarlTibule.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    //Added
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Utility;
    using System.Data;
    using System.Data.SqlClient;

    namespace BankOfBIT_CarlTibule.Models
    {
        /// <summary>
        /// Models a Client table in the database
        /// </summary>
        public class Client
        {
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int ClientId { get; set; }

            [Display(Name = "Client")]
            public long ClientNumber { get; set; }

            [Required]
            [StringLength(35, MinimumLength = 1)]
            [Display(Name = "First\nName")]
            public string FirstName { get; set; }

            [Required]
            [StringLength(35, MinimumLength = 1)]
            [Display(Name = "Last\nName")]
            public string LastName { get; set; }

            [Required]
            [StringLength(35, MinimumLength = 1)]
            [Display(Name = "Street\nAddress")]
            public string Address { get; set; }

            [Required]
            [StringLength(35, MinimumLength = 1)]
            [Display(Name = "City")]
            public string City { get; set; }

            [Required]
            [RegularExpression("^(N[BLSTU]|[AMN]B|[BQ]C|ON|PE|SK|YT)$", ErrorMessage = "A valid postal abbreviation for a Canadian province must be entered.")]
            public string Province { get; set; }

            [Required]
            [StringLength(7)]
            [RegularExpression(@"^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$", ErrorMessage="A valid Canadian postal code must be entered.")]
            [Display(Name = "Postal\nCode")]
            public string PostalCode { get; set; }

            [Required]
            [Display(Name = "Date\nCreated")]
            [DisplayFormat(DataFormatString = "{0:d}")]
            public DateTime DateCreated { get; set; }

            [Display(Name = "Client\nNotes")]
            public string Notes { get; set; }

            [Display(Name = "Name")]
            public string FullName { get { return String.Format("{0} {1}", FirstName, LastName); } }

            [Display(Name = "Address")]
            public string FullAddress { get { return String.Format("{0} {1}, {2} {3}", Address, City, Province, PostalCode); } }

            //Navigational Property: Clients can have 0 or more Bank Accounts
            public virtual ICollection<BankAccount> BankAccount { get; set; }

            /// <summary>
            /// Assigns ClientNumber to a new record
            /// </summary>
            public void SetNextClientNumber()
            {
                //Added Milestone #4: 2017-05-25. 
                this.ClientNumber = (long)StoredProcedure.NextNumber("NextClientNumbers");
            }
        }

        /// <summary>
        /// Models a BankAccount table in the database
        /// </summary>
        public abstract class BankAccount
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int BankAccountId { get; set; }

            [Display(Name = "Account\nNumber")]
            public long AccountNumber { get; set; }

            [Required]
            [ForeignKey("Client")]
            public int ClientId { get; set; }

            [Required]
            [ForeignKey("AccountState")]
            public int AccountStateId { get; set; }

            [Required]
            [Display(Name = "Current\nBalance")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double Balance { get; set; }

            [Required]
            [Display(Name = "Opening\nBalance")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double OpeningBalance { get; set; }

            [Required]
            [Display(Name = "Date\nCreated")]
            [DisplayFormat(DataFormatString = "{0:d}")]
            public DateTime DateCreated { get; set; }

            [Display(Name = "Account\nNotes")]
            public string Notes { get; set; }

            [Display(Name = "Account\nType")]
            public string Description { get { return FormatUtils.RemoveWord(this.GetType().Name, "Account"); } }

            //Navigational Properties
            //Bank Account can only have 1 client
            public virtual Client Client { get; set; }

            //Bank Account can only have 1 Account State
            public virtual AccountState AccountState { get; set; }

            //ADDED: 23 September 2017
            //Bank Account can be attached to 0 or more Transactions
            public virtual ICollection<Transaction> Transaction { get; set; }

            /// <summary>
            /// Assigns Account Number to a new Bank Account record
            /// </summary>
            public abstract void SetNextAccountNumber();

            /// <summary>
            /// Checks for the state of the Bank Account to determine whether its Account State needs to be changed.
            /// </summary>
            public void ChangeState()
            {
                AccountState currentState = null;
                AccountState newState = null;
                BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

                do
                {
                    //Keep track of the current account state abd call the state change check for that state
                    currentState = dataContext.AccountStates.Find(this.AccountStateId);
                    currentState.StateChangeCheck(this);

                    //Change value of newState to the (potentially new) account state of the bank account.
                    newState = dataContext.AccountStates.Find(this.AccountStateId);

                } while (currentState != newState);
            }
        }

        /// <summary>
        /// An entity that derives from BankAccount
        /// </summary>
        public class SavingsAccount : BankAccount
        {
            [Required]
            [Display(Name = "Service\nCharges")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double SavingsServiceCharges { get; set; }

            /// <summary>
            /// Assigns Account Number to a new Savings Account record
            /// </summary>
            public override void SetNextAccountNumber()
            {
                //Added: Milestone 4 [2017-09-25]
                this.AccountNumber = (long)StoredProcedure.NextNumber("NextSavingsAccounts");
            }
        }

        /// <summary>
        /// An entity that derives from BankAccount 
        /// </summary>
        public class InvestmentAccount : BankAccount
        {
            [Required]
            [Display(Name = "Interest\nRate")]
            [DisplayFormat(DataFormatString = "{0:P2}")]
            public double InterestRate { get; set; }

            /// <summary>
            /// Assigns Account Number to a new Investment Account record
            /// </summary>
            public override void SetNextAccountNumber()
            {
                //Added: Milestone 4 [2017-09-25]
                this.AccountNumber = (long)StoredProcedure.NextNumber("NextInvestmentAccounts");
            }
        }

        /// <summary>
        /// An entity that derives from BankAccount
        /// </summary>
        public class MortgageAccount : BankAccount
        {
            [Required]
            [Display(Name = "Interest\nRate")]
            [DisplayFormat(DataFormatString = "{0:P2}")]
            public double MortgageRate { get; set; }

            [Required]
            public int Amortization { get; set; }

            /// <summary>
            /// Assigns Account Number to a new Mortgage Account record
            /// </summary>
            public override void SetNextAccountNumber()
            {
                //Added: Milestone 4 [2017-09-25]
                this.AccountNumber = (long)StoredProcedure.NextNumber("NextMortgageAccounts");
            }
        }

        /// <summary>
        /// An entity that derives from BankAccount
        /// </summary>
        public class ChequingAccount : BankAccount
        {
            [Required]
            [Display(Name = "Service\nCharges")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double ChequingServiceCharges { get; set; }

            /// <summary>
            /// Assigns Account Number to a new Chequing Account record
            /// </summary>
            public override void SetNextAccountNumber()
            {
                //Added: Milestone 4 [2017-09-25]
                this.AccountNumber = (long)StoredProcedure.NextNumber("NextChequingAccounts");
            }
        }

        /// <summary>
        /// Models the AccountState table in the database
        /// </summary>
        public abstract class AccountState
        {
            //Data Context class
            protected static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int AccountStateId { get; set; }

            [Display(Name = "Lower\nLimit")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double LowerLimit { get; set; }

            [Display(Name = "Upper\nLimit")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double UpperLimit { get; set; }

            [Required]
            [Display(Name = "Interest\nRate")]
            [DisplayFormat(DataFormatString = "{0:P2}")]
            public double Rate { get; set; }

            [Display(Name = "Account\nState")]
            public string Description { get { return FormatUtils.RemoveWord(this.GetType().Name, "State"); } }

            /// <summary>
            /// Adjusts the default interest rate associated with the state based on rules attached to it.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            /// <returns>Adjusted interest rate</returns>
            public virtual double RateAdjustment(BankAccount bankAccount)
            {
                return 0;
            }

            /// <summary>
            /// Checks the balance of the bank account object to determine if a change in state is needed.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            public virtual void StateChangeCheck(BankAccount bankAccount)
            { }
        }

        /// <summary>
        /// A subtype of AccountState
        /// </summary>
        public class BronzeState : AccountState
        {
            private static BronzeState bronzeState;

            /// <summary>
            /// Constructs an instance of BronzeState and sets the lower and upper limits as well as the rate.
            /// </summary>
            private BronzeState()
            {
                this.LowerLimit = 0;
                this.UpperLimit = 5000;
                this.Rate = 0.01;
            }

            /// <summary>
            /// Checks if an instance of BronzeState is present in the databse and if not, creates an instance of it and adds it to the database
            /// </summary>
            /// <returns>An instance of BronzeState</returns>
            public static BronzeState GetInstance()
            {
                if (bronzeState == null)
                {
                    bronzeState = dataContext.BronzeStates.SingleOrDefault();

                    if (bronzeState == null)
                    {
                        bronzeState = new BronzeState();
                        dataContext.BronzeStates.Add(bronzeState);
                        dataContext.SaveChanges();
                    }
                }

                return bronzeState;
            }

            /// <summary>
            /// Adjusts the interest rate of this state depending on the balance of the bank account.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            /// <returns>Interest Rate applied</returns>
            public override double RateAdjustment(BankAccount bankAccount)
            {
                double newRate = this.Rate;

                if (bankAccount.Balance <= 0)
                {
                    newRate = 0.055;
                }

                return newRate;
            }

            /// <summary>
            /// Checks the balance of the bank account to determine whether the account needs to be promoted 
            /// to SilverState, with the exception of mortgage accounts.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            public override void StateChangeCheck(BankAccount bankAccount)
            {
                //If the Bank Account is not Mortgage Account and balance exceeds upper limit, promote it to Silver State.
                if(!(bankAccount is MortgageAccount) && bankAccount.Balance > this.UpperLimit)
                {
                    bankAccount.AccountStateId = SilverState.GetInstance().AccountStateId;
                }
            }
        }

        /// <summary>
        /// A subtype of AccountState
        /// </summary>
        public class SilverState : AccountState
        {
            private static SilverState silverState;

            /// <summary>
            /// Constructs an instance of SilverState and sets the lower and upper limits as well as the rate.
            /// </summary>
            private SilverState()
            {
                this.LowerLimit = 5000;
                this.UpperLimit = 10000;
                this.Rate = 0.0125;
            }

            /// <summary>
            /// Checks if an instance of SilverState is present in the databse and if not, creates an instance of it and adds it to the database
            /// </summary>
            /// <returns>An instance of Silverstate object</returns>
            public static SilverState GetInstance()
            {
                if (silverState == null)
                {
                    silverState = dataContext.SilverStates.SingleOrDefault();

                    if (silverState == null)
                    {
                        silverState = new SilverState();
                        dataContext.SilverStates.Add(silverState);
                        dataContext.SaveChanges();
                    }
                }

                return silverState;
            }

            /// <summary>
            /// Sets the interest rate associate equivalent to the Rate property of this state
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            /// <returns>Interest Rate</returns>
            public override double RateAdjustment(BankAccount bankAccount)
            {
                return this.Rate;
            }

            /// <summary>
            /// Checks the balance of the bank account to determine if the bank account needs to be promoted to GoldState or demoted to BronzeState,
            /// with the exception of Mortgage Accounts.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            public override void StateChangeCheck(BankAccount bankAccount)
            {
                //Prevents Mortgage Account from being included in state change check
                if (!(bankAccount is MortgageAccount))
                { 
                    //If the balance is below lower limit, demote the bank account to bronze state.
                    if (bankAccount.Balance < this.LowerLimit)
                    {
                        bankAccount.AccountStateId = BronzeState.GetInstance().AccountStateId;
                    }
                    //If the balance is above the upper limit, promote the bank account to gold state.
                    else if (bankAccount.Balance > this.UpperLimit)
                    {
                        bankAccount.AccountStateId = GoldState.GetInstance().AccountStateId;
                    }
                }
            }
        }

        /// <summary>
        /// A subtype of Account State and sets the lower and upper limits as well as the rate.
        /// </summary>
        public class GoldState : AccountState
        {
            private static GoldState goldState;

            /// <summary>
            /// Constructs an instance of GoldState object 
            /// </summary>
            private GoldState()
            {
                this.LowerLimit = 10000;
                this.UpperLimit = 20000;
                this.Rate = 0.02;
            }

            /// <summary>
            /// Checks if an instance of GoldState is present in the databse and if not, creates an instance of it and adds it to the database
            /// </summary>
            /// <returns>instance of GoldState object</returns>
            public static GoldState GetInstance()
            {
                if (goldState == null)
                {
                    goldState = dataContext.GoldStates.SingleOrDefault();

                    if (goldState == null)
                    {
                        goldState = new GoldState();
                        dataContext.GoldStates.Add(goldState);
                        dataContext.SaveChanges();
                    }
                }

                return goldState;
            }

            /// <summary>
            /// Adjusts the interest rate of the bank account associated with this state based on the age of the account.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            /// <returns>Adjusted interest rate</returns>
            public override double RateAdjustment(BankAccount bankAccount)
            {
                double newRate = this.Rate;

                //Checks if the bank account was created 10 years ago (or older), 
                if (Numeric.IsOlderThan(bankAccount.DateCreated, 10))
                {
                    newRate += 0.01;
                }

                return newRate;
            }

            /// <summary>
            /// Checks the balance of the bank account to determine if it needs to be demoted to SilverState or promoted to PlatinumState, except
            /// for mortgage accounts.
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            public override void StateChangeCheck(BankAccount bankAccount)
            {
                //Exclude mortgage accounts from state change check
                if(!(bankAccount is MortgageAccount))
                {
                    //Demote account state to SilverState if balance is below the lower limit
                    if (bankAccount.Balance < this.LowerLimit)
                    {
                        bankAccount.AccountStateId = SilverState.GetInstance().AccountStateId;
                    }
                    //Promote account state to PlatinumState if balance is above the upper limit
                    else if (bankAccount.Balance > this.UpperLimit)
                    {
                        bankAccount.AccountStateId = PlatinumState.GetInstance().AccountStateId;
                    }
                }
            }
        }

        /// <summary>
        /// A subtype of AccountState
        /// </summary>
        public class PlatinumState : AccountState
        {
            private static PlatinumState platinumState;

            /// <summary>
            /// Constructs an instance of PlatinumState and sets the lower and upper limits as well as the rate.
            /// </summary>
            private PlatinumState()
            {
                this.LowerLimit = 20000;
                this.UpperLimit = 0;
                this.Rate = 0.0250;
            }

            /// <summary>
            /// Checks if an instance of PlatinumState is present in the databse and if not, creates an instance of it and adds it to the database
            /// </summary>
            /// <returns>An instance of PlatinumState</returns>
            public static PlatinumState GetInstance()
            {
                if (platinumState == null)
                {
                    platinumState = dataContext.PlatinumStates.SingleOrDefault();

                    if (platinumState == null)
                    {
                        platinumState = new PlatinumState();
                        dataContext.PlatinumStates.Add(platinumState);
                        dataContext.SaveChanges();
                    }
                }

                return platinumState;
            }

            /// <summary>
            /// Adjusts the rate applied to the bank account associated with this state based on the age and the balance of the bank account
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            /// <returns>Adjusted Interest Rate</returns>
            public override double RateAdjustment(BankAccount bankAccount)
            {
                double newRate = this.Rate;

                //Add 1% to the rate applicable if the bank account is 10 years old or older.
                if(Numeric.IsOlderThan(bankAccount.DateCreated, 10))
                {
                    newRate += 0.01;
                }

                //Add 0.5% to the rate applicable if the bank account's balance is 2x the lower limit
                if (bankAccount.Balance > (this.LowerLimit * 2))
                {
                    newRate += 0.005;
                }

                return newRate;
            }

            /// <summary>
            /// Checks the balance of the bank account to determine if the account needs to be demoted to GoldState
            /// </summary>
            /// <param name="bankAccount">A bank account object</param>
            public override void StateChangeCheck(BankAccount bankAccount)
            { 
                //Exclude mortgage account from state change check and demote it if the balance is below lower limit
                if(!(bankAccount is MortgageAccount) && bankAccount.Balance < this.LowerLimit)
                {
                    bankAccount.AccountStateId = GoldState.GetInstance().AccountStateId;
                }
            }
        }

        /// <summary>
        /// Models the NextTransactionNumber table in the database
        /// </summary>
        public class NextTransactionNumber
        {
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            public int NextTransactionNumberId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextTransactionNumber nextTransactionNumber;

            /// <summary>
            /// Constructs an instance of this object and sets the value of NextAvailableNumber to 700.
            /// </summary>
            private NextTransactionNumber()
            {
                this.NextAvailableNumber = 700;
            }

            /// <summary>
            /// Checks if an instance of NextTransactionNumber is in the database and if not, creates an instance of it and adds it to the database
            /// </summary>
            /// <returns>Next Transaction Number in-queue to be assigned</returns>
            public static NextTransactionNumber GetInstance()
            {
                if (nextTransactionNumber == null)
                {
                    nextTransactionNumber = dataContext.NextTransactionNumbers.SingleOrDefault();

                    if (nextTransactionNumber == null)
                    {
                        nextTransactionNumber = new NextTransactionNumber();
                        dataContext.NextTransactionNumbers.Add(nextTransactionNumber);
                        dataContext.SaveChanges();
                    }
                }

                return nextTransactionNumber;
            }
        }

        /// <summary>
        /// Models the NextClientNumber table in the database
        /// </summary>
        public class NextClientNumber
        {
            //Private Instance of Database Class
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int NextClientNumberId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextClientNumber nextClientNumber;

            /// <summary>
            /// Constructs an instance of this object and sets the value of the NextAvailableNumber
            /// </summary>
            private NextClientNumber()
            {
                this.NextAvailableNumber = 20000000;
            }

            /// <summary>
            /// Checks in an instance of NextClientNumber is in database and if not, it creates one and adds it to the database.
            /// </summary>
            /// <returns>Next Client Number in-queue to be assigned</returns>
            public static NextClientNumber GetInstance()
            {
                if (nextClientNumber == null)
                {
                    nextClientNumber = dataContext.NextClientNumbers.SingleOrDefault();

                    if (nextClientNumber == null)
                    {
                        nextClientNumber = new NextClientNumber();
                        dataContext.NextClientNumbers.Add(nextClientNumber);
                        dataContext.SaveChanges();
                    }
                }

                return nextClientNumber;
            }
        }

        /// <summary>
        /// Models NextSavingsAccount table in the database
        /// </summary>
        public class NextSavingsAccount
        {
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int NextSavingsAccountId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextSavingsAccount nextSavingsAccount;

            /// <summary>
            /// Constructs an instance of this object and sets the NextAvailableNumber attribute to 20000
            /// </summary>
            private NextSavingsAccount()
            {
                this.NextAvailableNumber = 20000;
            }

            /// <summary>
            /// Checks if there's an instance of NextSavingsAccount object and in the database and if not, creates one and stores it in the database
            /// </summary>
            /// <returns>An instance of NextSavingsAccount</returns>
            public static NextSavingsAccount GetInstance()
            {
                if (nextSavingsAccount == null)
                {
                    nextSavingsAccount = dataContext.NextSavingsAccounts.SingleOrDefault();

                    if (nextSavingsAccount == null)
                    {
                        nextSavingsAccount = new NextSavingsAccount();
                        dataContext.NextSavingsAccounts.Add(nextSavingsAccount);
                        dataContext.SaveChanges();
                    }
                }

                return nextSavingsAccount;
            }
        }

        /// <summary>
        /// Models NextMortgageAccount model in the database
        /// </summary>
        public class NextMortgageAccount
        {
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int NextMortgageAccountId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextMortgageAccount nextMortgageAccount;

            /// <summary>
            /// Constructs an instance of this object
            /// </summary>
            private NextMortgageAccount()
            {
                this.NextAvailableNumber = 200000;
            }

            /// <summary>
            /// Checks if an instance of NextMortgageAccount is in the database and if not, creates one and stores it in the database
            /// </summary>
            /// <returns>An instance of this object</returns>
            public static NextMortgageAccount GetInstance()
            {
                if (nextMortgageAccount == null)
                {
                    nextMortgageAccount = dataContext.NextMortgageAccounts.SingleOrDefault();

                    if (nextMortgageAccount == null)
                    {
                        nextMortgageAccount = new NextMortgageAccount();
                        dataContext.NextMortgageAccounts.Add(nextMortgageAccount);
                        dataContext.SaveChanges();
                    }
                }

                return nextMortgageAccount;
            }
        }

        /// <summary>
        /// Models NextInvestmentAccount in the database
        /// </summary>
        public class NextInvestmentAccount
        {
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int NextInvestmentAccountId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextInvestmentAccount nextInvestmentAccount;

            /// <summary>
            /// Constructs an instance of this object
            /// </summary>
            private NextInvestmentAccount()
            {
                this.NextAvailableNumber = 2000000;
            }

            /// <summary>
            /// Checks if an instance of this object is in the database and if not, creates one and adds it to the database
            /// </summary>
            /// <returns>An instance of this object</returns>
            public static NextInvestmentAccount GetInstance()
            {
                if (nextInvestmentAccount == null)
                {
                    nextInvestmentAccount = dataContext.NextInvestmentAccounts.SingleOrDefault();

                    if (nextInvestmentAccount == null)
                    {
                        nextInvestmentAccount = new NextInvestmentAccount();
                        dataContext.NextInvestmentAccounts.Add(nextInvestmentAccount);
                        dataContext.SaveChanges();
                    }
                }

                return nextInvestmentAccount;
            }
        }

        /// <summary>
        /// Models NextChequingAccount table in the database
        /// </summary>
        public class NextChequingAccount
        {
            private static BankOfBIT_CarlTibuleContext dataContext = new BankOfBIT_CarlTibuleContext();

            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int NextChequingAccountId { get; set; }

            public long NextAvailableNumber { get; set; }

            private static NextChequingAccount nextChequingAccount;

            /// <summary>
            /// Constructs an instance of this object
            /// </summary>
            private NextChequingAccount()
            {
                this.NextAvailableNumber = 20000000;
            }

            /// <summary>
            /// Checks if an instance of this object is in database and if not, it creates one and add it to the database
            /// </summary>
            /// <returns>Instance of this object</returns>
            public static NextChequingAccount GetInstance()
            {
                if (nextChequingAccount == null)
                {
                    nextChequingAccount = dataContext.NextChequingAccounts.SingleOrDefault();

                    if (nextChequingAccount == null)
                    {
                        nextChequingAccount = new NextChequingAccount();
                        dataContext.NextChequingAccounts.Add(nextChequingAccount);
                        dataContext.SaveChanges();
                    }
                }

                return nextChequingAccount;
            }
        }

        /// <summary>
        /// Models the Payee table in the Database
        /// </summary>
        public class Payee
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int PayeeId { get; set; }

            [Display(Name = "Payee")]
            public string Description { get; set; }
        }

        /// <summary>
        /// Models the Institution table in the Database
        /// </summary>
        public class Institution
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int InstitutionId { get; set; }

            [Display(Name = "Institution\nNumber")]
            public int InstitutionNumber { get; set; }

            [Display(Name = "Institution")]
            public string Description { get; set; }
        }

        /// <summary>
        /// Models the Transacton table in the Database
        /// </summary>
        public class Transaction
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int TransactionId { get; set; }

            [Display(Name="Transaction\nNumber")]
            public long TransactionNumber { get; set; }

            [Required]
            [ForeignKey("BankAccount")]
            public int BankAccountId { get; set; }

            [Required]
            [ForeignKey("TransactionType")]
            public int TransactionTypeId { get; set; }

            [Display(Name="Deposit")]
            [DisplayFormat(DataFormatString="{0:C}")]
            public double Deposit { get; set; }

            [Display(Name="Withdrawal")]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public double Withdrawal { get; set; }

            [Required]
            [Display(Name="Date\nCreated")]
            public DateTime DateCreated { get; set; }

            [Display(Name="Notes")]
            public string Notes { get; set; }

            //Navigational Properties
            //A Transaction is attached to 1 Bank Account
            public virtual BankAccount BankAccount { get; set; }

            //A Transaction is attached to only 1 TransactionType
            public virtual TransactionType TransactionType { get; set; }

            /// <summary>
            /// Sets the Next Transaction Number
            /// </summary>
            public void SetNextTransactionNumber()
            { 
                //Added Milestone 4: 2017-09-25
                this.TransactionNumber = (long)StoredProcedure.NextNumber("NextTransactionNumbers");
            }
        }

        /// <summary>
        /// Models the TransactionType table in the Database
        /// </summary>
        public class TransactionType
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int TransactionTypeId { get; set; }

            [Display(Name="Transaction\nType")]
            public string Description { get; set; }

            [Display(Name="Service\nCharges")]
            [DisplayFormat(DataFormatString="{0:C}")]
            public double ServiceCharges { get; set; }
        }

        public class RFIDTag
        {
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RFIDTagId { get; set; }

            [Display(Name="CardId")]
            public long CardNumber { get; set; }

            [Required]
            [ForeignKey("Client")]
            [Display(Name="Client")]
            public int ClientId { get; set; }

            //Navigational Property: RFIDTag is associated with only 1 Client
            public virtual Client Client { get; set; }
        }

        /// <summary>
        /// Static class that contains functions that executes the Stored Procedures
        /// </summary>
        public static class StoredProcedure
        {
            /// <summary>
            /// Executes the stored procedure that creates the next number
            /// </summary>
            /// <param name="tableName">The table where the stored procedure will be applied to</param>
            /// <returns>Next number in queue to be assigned</returns>
            public static long? NextNumber(string tableName)
            {
                try
                {
                    //Define connection to the database
                    SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=BankOfBIT_CarlTibuleContext;Integrated Security=True");

                    //Define StoredProcedure and attach a connection to the database to it (Context)
                    SqlCommand storedProcedure = new SqlCommand("next_number", connection);

                    //Define the nature of its CommandType
                    storedProcedure.CommandType = CommandType.StoredProcedure;

                    //Pass tableName as an input parameter to storedProcedure
                    storedProcedure.Parameters.AddWithValue("@TableName", tableName);

                    //Define output parameter
                    SqlParameter outputParameter = new SqlParameter("@NewVal", SqlDbType.BigInt) { Direction = ParameterDirection.Output };

                    //Add output parameter to storedProcedure
                    storedProcedure.Parameters.Add(outputParameter);

                    //Open the connection
                    connection.Open();

                    //Execute the storedProcedure command
                    storedProcedure.ExecuteNonQuery();

                    //Close the connection
                    connection.Close();

                    return (long?)outputParameter.Value;
                }
                catch (Exception)
                {
                    //Catch exceptions and return null
                    return null;
                }
            }
        }
    }
}