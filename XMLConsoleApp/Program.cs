﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;
using WindowsBankingApplication;
using System.Configuration;

//added 
using System.IO;

namespace XMLConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xmlFile = XDocument.Load("2017-320-34439.xml");
            XElement rootElement = xmlFile.Element("account_update");

            //Try to get the sum
            long sum = rootElement.Descendants("account_no").Sum(x => int.Parse(x.Value));

            Console.WriteLine(sum);
            Console.WriteLine(rootElement.Elements("transaction").Count());
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
