﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

//Added 
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using Utility;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TransactionManager" in code, svc and config file together.
public class TransactionManager : ITransactionManager
{
    //Private instance of DataContext class
    private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

    /// <summary>
    /// Queries the database to find the bank account 
    /// </summary>
    /// <param name="bankAccountId">ID associated with the BankAccount</param>
    /// <returns>Bank Account</returns>
    private BankAccount getBankAccount(int bankAccountId)
    {
        return (from result in db.BankAccounts
                where result.BankAccountId == bankAccountId
                select result).SingleOrDefault();
    }

    /// <summary>
    /// Queries the database to find the transaction type
    /// </summary>
    /// <param name="transactionType">Description of Transaction Type</param>
    /// <returns>Transaction Type</returns>
    private TransactionType getTransactionType(TransactionTypeValues transacType)
    {
        return (from result in db.TransactionTypes
                where result.TransactionTypeId == (int)transacType
                select result).SingleOrDefault();
    }

    /// <summary>
    /// Updates the balance of the bank account using the arguments provided to deposit and withdrawal parameters
    /// </summary>
    /// <param name="account">The account where the balance will be updated.</param>
    /// <param name="deposit">The amount to deposit to the account</param>
    /// <param name="withdraw">The amount to withdraw from the account</param>
    private void updateBalance(BankAccount account, double deposit, double withdraw)
    { 
        //Deposit to account
        account.Balance += deposit;

        //Withdraw from account
        account.Balance -= withdraw;

        //Check the state of the BankAccount
        account.ChangeState();

        //Save updates to the database
        db.SaveChanges();
    }

    /// <summary>
    /// Creates a new entry to the Transactions table to record the transaction that took place
    /// </summary>
    /// <param name="account">The account that was involved in the transaction</param>
    /// <param name="transacType">Type of transaction</param>
    /// <param name="deposit">Amount deposited to the account</param>
    /// <param name="withdrawal">Amount withdrawn from the account</param>
    /// <param name="notes">Optional details about the transaction</param>
    private void createTransaction(BankAccount account, TransactionType transacType, double deposit, double withdrawal, string notes)
    { 
        //Create new transaction
        Transaction transaction = new Transaction();
        transaction.SetNextTransactionNumber();
        transaction.BankAccountId = account.BankAccountId;
        transaction.TransactionTypeId = transacType.TransactionTypeId;
        transaction.Deposit = deposit;
        transaction.Withdrawal = withdrawal;
        transaction.DateCreated = DateTime.Now;
        transaction.Notes = notes;
        transaction.BankAccount = account;
        transaction.TransactionType = transacType;
        
        //Add transaction to database
        db.Transactions.Add(transaction);
    
        //Save changes to the database
        db.SaveChanges();
    }

    /// <summary>
    /// Deposits cash to the bank account
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be deposited</param>
    /// <param name="amount">The amount deposited to the bank account</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    public double? Deposit(int accountId, double amount, string notes)
    {
        try
        {
            // Get BankAccount that matches the account ID
            BankAccount account = getBankAccount(accountId);

            //Get deposit transaction type
            TransactionType transactionType = getTransactionType(TransactionTypeValues.Deposit);

            //Update the balance
            updateBalance(account, amount, 0);

            //Make new transaction
            createTransaction(account, transactionType, amount, 0, notes);

            //Return the updated balance
            return account.Balance;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Withdraws cash from the bank account
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be withdrawn</param>
    /// <param name="amount">The amount of cash that will be withdrawn</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    public double? Withdrawal(int accountId, double amount, string notes)
    {
        try
        {
            //Get BankAccount that matches the account ID
            BankAccount account = getBankAccount(accountId);

            //Get withdrawal transaction type
            TransactionType transactionType = getTransactionType(TransactionTypeValues.Withdrawal);

            //Update the balance 
            updateBalance(account, 0, amount);

            //Create new transaction
            createTransaction(account, transactionType, 0, amount, notes);

            //Return the balance
            return account.Balance;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Subtracts the amount from the bank account to perform a bill payment
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be subtracted</param>
    /// <param name="amount">Amount that will be used to pay the bill</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    public double? BillPayment(int accountId, double amount, string notes)
    {
        try
        {
            //Get BankAccount that matches the account ID
            BankAccount account = getBankAccount(accountId);

            //Get bill payment transaction type
            TransactionType transactionType = getTransactionType(TransactionTypeValues.BillPayment);

            //Update the balance 
            updateBalance(account, 0, amount);

            //Create new transaction
            createTransaction(account, transactionType, 0, amount, notes);

            //Return the balance
            return account.Balance;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Transfers the amount from one bank account to another
    /// </summary>
    /// <param name="fromAccountId">Bank account where the cash will be transferred from</param>
    /// <param name="toAccountId">Bank account where the cash will be transferred to</param>
    /// <param name="amount">The amount of cash that will be transferred</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    public double? Transfer(int fromAccountId, int toAccountId, double amount, string notes)
    {
        try
        {
            //For the Origin Account
            BankAccount originAccount = getBankAccount(fromAccountId);
            TransactionType originTransacType = getTransactionType(TransactionTypeValues.Transfer);
            updateBalance(originAccount, 0, amount);
            createTransaction(originAccount, originTransacType, 0, amount, notes);
            
            //For the Destination Account
            BankAccount destAccount = getBankAccount(toAccountId);
            TransactionType destTransacType = getTransactionType(TransactionTypeValues.TransferRecipient);
            updateBalance(destAccount, amount, 0);
            createTransaction(destAccount, destTransacType, amount, 0, notes);

            //Return updated balance of the origin account
            return originAccount.Balance;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Calculates the interest rate of the bank account based on its account state
    /// </summary>
    /// <param name="accountId">Unique ID for a bank account</param>
    /// <param name="notes">Detail about the transaction</param>
    /// <returns>Interest on the bank account</returns>
    public double? CalculateInterest(int accountId, string notes)
    {
        BankAccount bankAccount = getBankAccount(accountId);

        //Get the necessary variables to calculate the interest rate
        double interestRate = bankAccount.AccountState.RateAdjustment(bankAccount);
        double balance = bankAccount.Balance;
        int interestApplicationFrequency = 12;

        //Get the number of time periods elapsed dince the last interest calculation
        int timePeriodsElapsed = 1;

        //Calculate the interest
        double calculatedInterest = Math.Round((interestRate * balance * timePeriodsElapsed) / interestApplicationFrequency, 2);

        //Variable to store updated balance
        double? updatedBalance = 0;

        //Deposit interest if the calculated interest is positive
        if (calculatedInterest > 0)
        {
            updatedBalance = this.Deposit(accountId, calculatedInterest, notes);
        }
        //Withdraw interest if calculated interest is positive
        else if(calculatedInterest < 0)
        {
            updatedBalance = this.Withdrawal(accountId, Math.Abs(calculatedInterest), notes);
        }

        return updatedBalance;
    }
}