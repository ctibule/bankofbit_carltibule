﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITransactionManager" in both code and config file together.
[ServiceContract]
public interface ITransactionManager
{
    /// <summary>
    /// Deposits cash to the bank account
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be deposited</param>
    /// <param name="amount">The amount deposited to the bank account</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    [OperationContract]
    double? Deposit(int accountId, double amount, string notes);

    /// <summary>
    /// Withdraws cash from the bank account
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be withdrawn</param>
    /// <param name="amount">The amount of cash that will be withdrawn</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    [OperationContract]
    double? Withdrawal(int accountId, double amount, string notes);

    /// <summary>
    /// Subtracts the amount from the bank account to perform a bill payment
    /// </summary>
    /// <param name="accountId">Bank account where the amount will be subtracted</param>
    /// <param name="amount">Amount that will be used to pay the bill</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    [OperationContract]
    double? BillPayment(int accountId, double amount, string notes);

    /// <summary>
    /// Transfers the amount from one bank account to another
    /// </summary>
    /// <param name="fromAccountId">Bank account where the cash will be transferred from</param>
    /// <param name="toAccountId">Bank account where the cash will be transferred to</param>
    /// <param name="amount">The amount of cash that will be transferred</param>
    /// <param name="notes">Details about the transaction</param>
    /// <returns>Updated balance. NULL if an exception occurs.</returns>
    [OperationContract]
    double? Transfer(int fromAccountId, int toAccountId, double amount, string notes);

    /// <summary>
    /// * ENTER DESCRIPTION HERE*
    /// </summary>
    /// <param name="accountId"></param>
    /// <param name="notes"></param>
    /// <returns></returns>
    [OperationContract]
    double? CalculateInterest(int accountId, string notes);
}
