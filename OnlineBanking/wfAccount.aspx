﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="wfAccount.aspx.cs" Inherits="wfAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>
        <asp:Label ID="lblClient" runat="server"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblAccountNumber" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblBalance" runat="server"></asp:Label>
    </p>
    <p>
        <asp:GridView ID="gvTransactions" runat="server" AutoGenerateColumns="False" 
            Width="751px">
            <Columns>
                <asp:BoundField DataField="DateCreated" DataFormatString="{0:d}" 
                    HeaderText="Date">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="TransactionType.Description" 
                    HeaderText="Transaction Type">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Deposit" DataFormatString="{0:C}" 
                    HeaderText="Amount In">
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="Withdrawal" DataFormatString="{0:C}" 
                    HeaderText="Amount Out">
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="Notes" HeaderText="Details">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
    </p>
    <p>
        <asp:LinkButton ID="lnkPayTransfer" runat="server" 
            onclick="lnkPayTransfer_Click" Visible="False">Pay Bills and Transfer Funds</asp:LinkButton>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" 
            Text="To display error/exception messages" Visible="False"></asp:Label>
    </p>
</asp:Content>

