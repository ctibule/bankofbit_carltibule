﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//added
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using Utility;

public partial class wfAccount : System.Web.UI.Page
{
    /// <summary>
    /// Handles page load event
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (HttpContext.Current.User.Identity.Name == "")
            {
                throw new NotLoggedInException();
            }
            else if(Session["BankAccountId"] == null || Session["AccountNumber"] == null || Session["Balance"] == null)
            {
                throw new NoBankAccountException();
            }
            else if(!IsPostBack)
            {
                //Make the hyperlink visible
                lnkPayTransfer.Visible = true;

                //Define data context
                BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

                //Populate labels with session variables
                lblClient.Text = "Client:\t" + Session["FullName"].ToString();
                lblAccountNumber.Text = "Account Number:\t" + Session["AccountNumber"].ToString();
                lblBalance.Text = "Balance:\t" + Session["Balance"].ToString();

                //Populate the DataGridView with transactions attached to the BankAccount
                int bankAccountId = int.Parse(Session["BankAccountId"].ToString());
                IQueryable<Transaction> transactions = from results in db.Transactions
                                                       where results.BankAccountId == bankAccountId
                                                       select results;
                gvTransactions.DataSource = transactions.ToList();

                //Bind controls
                this.DataBind();
            }
            
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = "An error occured: " + ex.Message;
        }
    }

    /// <summary>
    /// Handles Click event for Linked Label control
    /// </summary>
    protected void lnkPayTransfer_Click(object sender, EventArgs e)
    {
        Server.Transfer("wfTransaction.aspx");
    }
}