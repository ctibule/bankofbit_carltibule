﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="wfClient.aspx.cs" Inherits="wfClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Label ID="lblClient" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;
    <br />
    <br />
    <asp:GridView ID="gvBankAccounts" runat="server" AutoGenerateColumns="False" 
        Width="704px" AutoGenerateSelectButton="True" 
        onselectedindexchanged="gvBankAccounts_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="AccountNumber" HeaderText="Account Number" />
            <asp:BoundField DataField="Notes" HeaderText="Account Notes" />
            <asp:BoundField DataField="Balance" DataFormatString="{0:C}" 
                HeaderText="Balance">
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <asp:Label ID="lblExchangeRate" runat="server"></asp:Label>
    <br />
    <asp:Label ID="lblError" runat="server" ForeColor="Red" 
        Text="To display error/exception messages" Visible="False"></asp:Label>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

