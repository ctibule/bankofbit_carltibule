﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//added
using Utility;
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using TransactionManager;

public partial class wfTransaction : System.Web.UI.Page
{
    private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

    /// <summary>
    /// Utilizes the Web Service to apply appropriate transaction to affected payee/account according to user selection
    /// </summary>
    /// <param name="amountToApply">Amount to apply to the transaction</param>
    private string applyTransaction(double amountToApply)
    {
        //Declare an instance of Web Service
        TransactionManagerClient transactionManager = new TransactionManagerClient();

        //Declare output variable
        double? updatedBalance = 0;

        //Declare parameters to be passed
        int bankAccountId = int.Parse(Session["BankAccountId"].ToString());
        double amount = amountToApply;
        string notes = "";

        //Check if selected transaction type is Bill Payment
        if (int.Parse(cboTransactionType.SelectedValue) == (int)TransactionTypeValues.BillPayment)
        {
            //Edit notes specifically for Bill Payment transaction
            notes = string.Format("Bill Payment to {0}", cboRecipient.SelectedItem.Text);

            //Apply transaction
            updatedBalance = transactionManager.BillPayment(bankAccountId, amount, notes);
        }
        //Check if selected transaction type is transfer
        else if (int.Parse(cboTransactionType.SelectedValue) == (int)TransactionTypeValues.Transfer)
        { 
            //Get destination BankAccountId
            int destBankAccountId = int.Parse(cboRecipient.SelectedValue);

            //Edit notes specifically for Transfer transaction
            notes = string.Format("Transfer of {0:C} to Account #{1}", amount, cboRecipient.SelectedItem.Text);

            //Apply transaction
            updatedBalance = transactionManager.Transfer(bankAccountId, destBankAccountId, amount, notes);
        }

        //Throw an exception if the WebService function returns null, which means the transaction failed.
        if (updatedBalance == null)
        {
            throw new TransactionFailedException();
        }
        //If the transaction was a success, update the Balance Session and lblBalance
        else
        {
            Session["Balance"] =  string.Format("{0:C}", updatedBalance);
            return Session["Balance"].ToString();
        }
    }

    /// <summary>
    /// Populates cboRecipient depending on the data source provided to this function
    /// </summary>
    /// <param name="dataSource">The pool of values that will be used to supply the drop down list</param>
    private void populateCboRecipient()
    {
        //Empty drop down list
        cboRecipient.DataSource = null;
        cboRecipient.DataTextField = null;
        cboRecipient.DataValueField = null;

        //Load the drop down list with Payees if Bill Payment is selected in cboTransactionType
        if (int.Parse(cboTransactionType.SelectedValue.ToString()) == (int)TransactionTypeValues.BillPayment)
        {
            IQueryable<Payee> payees = from x in db.Payees
                                       select x;
            cboRecipient.DataSource = payees.ToList();
            cboRecipient.DataTextField = "Description";
            cboRecipient.DataValueField = "PayeeId";
        }
        //Load the drop down list with BankAccounts if Transfer is selected in cboTransactionType
        else if (int.Parse(cboTransactionType.SelectedValue.ToString()) == (int)TransactionTypeValues.Transfer)
        {
            //Get current bank accoount
            int bankAccountId = int.Parse(Session["BankAccountId"].ToString());
            BankAccount currentBankAccount = (from x in db.BankAccounts
                                              where x.BankAccountId == bankAccountId
                                              select x).SingleOrDefault();
           
            //Get list of BankAccounts associated with this Client with the exception of this BankAccount
            IQueryable<BankAccount> bankAccounts = from x in db.BankAccounts
                                                   where x.ClientId == currentBankAccount.ClientId && x.BankAccountId != currentBankAccount.BankAccountId
                                                   select x;
            cboRecipient.DataSource = bankAccounts.ToList();
            cboRecipient.DataTextField = "AccountNumber";
            cboRecipient.DataValueField = "BankAccountId";
        }
    }

    /// <summary>
    /// Handles page load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if(HttpContext.Current.User.Identity.Name == "")
            {
                throw new NotLoggedInException();
            }
            else if (Session["BankAccountId"] == null || Session["AccountNumber"] == null || Session["Balance"] == null)
            {
                throw new NoBankAccountException();
            }
            else if (!IsPostBack)
            {
                //Right-align textbox
                txtAmount.Style["text-align"] = "right";

                //Populate labels with Session variables
                string currentBalance = Session["Balance"].ToString();
                lblAccountNumber.Text = "Acount Number: " + Session["AccountNumber"].ToString();
                lblBalance.Text = string.Format("Balance: {0:C}", currentBalance);

                //Populate cboTransactionType
                IQueryable<TransactionType> transactionTypes = from x in db.TransactionTypes
                                                               where x.TransactionTypeId == (int)TransactionTypeValues.BillPayment || 
                                                                        x.TransactionTypeId == (int)TransactionTypeValues.Transfer
                                                               select x;
                cboTransactionType.DataSource = transactionTypes.ToList();
                cboTransactionType.DataTextField = "Description";
                cboTransactionType.DataValueField = "TransactionTypeId";

                //Set default value
                cboTransactionType.SelectedValue = ((int)TransactionTypeValues.BillPayment).ToString();

                //Bind control
                this.DataBind();

                //Populate cboRecipient
                populateCboRecipient();

                //Bind control (for cboRecipient)
                this.DataBind();
            }  
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = "Error occurred: " + ex.Message;

            //Hide controls when Exception is caught
            lblAccountNumber.Visible = false;
            lblBalance.Visible = false;
            Label1.Visible = false;
            Label2.Visible = false;
            Label3.Visible = false;
            cboRecipient.Visible = false;
            cboTransactionType.Visible = false;
            txtAmount.Visible = false;
            btnCompleteTransaction.Visible = false;
        }
    }

    /// <summary>
    /// Handles Click event for the Complete button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCompleteTransaction_Click(object sender, EventArgs e)
    {
        try
        {
            //Only proceed through if the page is valid
            if (Page.IsValid)
            {
                //Compare the balance to the amount entered, throw an exception if amount entered is bigger
                double currentBalance = double.Parse(Session["Balance"].ToString().Replace("$", ""));
                double amountEntered = double.Parse(txtAmount.Text);

                //Throw exception if amount entered is greater than the balance. Otherwise, apply the transaction
                if (amountEntered > currentBalance)
                {
                    throw new InsufficientFundsException();
                }
                else
                {
                    //Apply transaction and lblBalance
                    lblBalance.Text = string.Format("Balance: {0}", applyTransaction(amountEntered));
                }

                //If errors were visible, hide it again
                lblError.Visible = false;
                lblError.Text = "";
            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = "Error occurred: " + ex.Message;
        }
    }

    /// <summary>
    /// Handles Selected Index Changed event for the Transaction type combobox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
    {
        populateCboRecipient();
        this.DataBind();
    }
}