﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//Added
using BankOfBIT_CarlTibule.Models;
using BankOfBIT_CarlTibule.Models.BankOfBIT_CarlTibule.Models;
using net.webservicex.www;
using Utility;
public partial class wfClient : System.Web.UI.Page
{
    //Declare global, private final variable that represents context class
    private BankOfBIT_CarlTibuleContext db = new BankOfBIT_CarlTibuleContext();

    /// <summary>
    /// Handles Page_Load Event
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (HttpContext.Current.User.Identity.Name == "")
            {
                throw new NotLoggedInException();
            }
            else if (IsPostBack == false)
            {
                //Declare username
                Session["Username"] = System.Web.HttpContext.Current.User.Identity.Name;

                //Get full name, store it in session and set the value of lblClient
                long clientNumber = long.Parse(Session["Username"].ToString());
                Client client = (from results in db.Clients
                                 where results.ClientNumber == clientNumber
                                 select results).SingleOrDefault();
                Session["FullName"] = client.FullName;
                lblClient.Text = "Client: " + Session["FullName"].ToString();

                //Set DataSource for GridView
                IQueryable<BankAccount> accounts = from results in db.BankAccounts
                                                   where results.Client.ClientNumber == clientNumber
                                                   select results;
                gvBankAccounts.DataSource = accounts.ToList();

                //Set label for currency conversion
                CurrencyConvertor convertor = new CurrencyConvertor();
                lblExchangeRate.Text = string.Format("The exchange rate between Canada and the United States is currently {0:C}.", convertor.ConversionRate(Currency.USD, Currency.CAD));

                //Bind controls
                this.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "An error occured: " + ex.Message;
            lblError.Visible = true;
        }
    }

    /// <summary>
    /// Handles changes in Selected Index in GridView control
    /// </summary>
    protected void gvBankAccounts_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Store account number and balance in session variables
        Session["AccountNumber"] = gvBankAccounts.Rows[gvBankAccounts.SelectedIndex].Cells[1].Text;
        Session["Balance"] = gvBankAccounts.Rows[gvBankAccounts.SelectedIndex].Cells[3].Text;

        //Get the BankAccountId that corresponds with the AccountNumber
        long accountNumber = long.Parse(Session["AccountNumber"].ToString());
        BankAccount account = (from result in db.BankAccounts
                               where result.AccountNumber == accountNumber
                               select result).SingleOrDefault();

        //Store BankAccountId to a Session variable
        Session["BankAccountId"] = account.BankAccountId;

        //Transfer to wfAccount.aspx
        Server.Transfer("wfAccount.aspx");
    }
}