﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="wfTransaction.aspx.cs" Inherits="wfTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>
        <br />
        <asp:Label ID="lblAccountNumber" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblBalance" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label1" runat="server" Text="Transaction Type:"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="cboTransactionType" runat="server" AutoPostBack="True" 
            Width="115px" 
            onselectedindexchanged="cboTransactionType_SelectedIndexChanged">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Text="Amount:"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtAmount" runat="server" CausesValidation="True" 
            Width="162px"></asp:TextBox>
        <asp:RangeValidator ID="rvTxtAmount" runat="server" 
            ControlToValidate="txtAmount" 
            ErrorMessage="Value entered for Amount textbox must be a numeric value between 0.01 to 10,000" 
            MaximumValue="10000" MinimumValue="0.01" Type="Double" ForeColor="Red"></asp:RangeValidator>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Text="To:"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="cboRecipient" runat="server" AutoPostBack="True" 
            Width="199px">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Button ID="btnCompleteTransaction" runat="server" 
            onclick="btnCompleteTransaction_Click" Text="Complete Transaction" 
            Width="240px" />
    </p>
    <p>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" 
            Visible="False"></asp:Label>
    </p>
    <p>
    </p>
    <p>
    </p>
</asp:Content>

