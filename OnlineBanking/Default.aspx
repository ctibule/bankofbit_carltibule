﻿<%--CHANGELOG
[2017-10-01] Modified as per A2M1 instructions @ p.3--%>

<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to Bank of BIT
    </h2>
    <p>
        Bank of BIT offers online banking features such as bill payment and money transfers.
    </p>
</asp:Content>
